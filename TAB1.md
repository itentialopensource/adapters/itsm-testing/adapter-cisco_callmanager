# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Cisco_callmanager System. The API that was used to build the adapter for Cisco_callmanager is usually available in the report directory of this adapter. The adapter utilizes the Cisco_callmanager API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Cisco Call Manager adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Cisco Call Manager. With this adapter you have the ability to perform operations such as:

- Add Phone
- Get Phone

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
