## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Cisco Call Manager. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Cisco Call Manager.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Cisco Call Manager. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">postAddSipProfile(body, callback)</td>
    <td style="padding:15px">addSipProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateSipProfile(body, callback)</td>
    <td style="padding:15px">updateSipProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetSipProfile(body, callback)</td>
    <td style="padding:15px">getSipProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveSipProfile(body, callback)</td>
    <td style="padding:15px">removeSipProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListSipProfile(body, callback)</td>
    <td style="padding:15px">listSipProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestartSipProfile(body, callback)</td>
    <td style="padding:15px">restartSipProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postApplySipProfile(body, callback)</td>
    <td style="padding:15px">applySipProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetSipProfileOptions(body, callback)</td>
    <td style="padding:15px">getSipProfileOptions</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddSipTrunkSecurityProfile(body, callback)</td>
    <td style="padding:15px">addSipTrunkSecurityProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateSipTrunkSecurityProfile(body, callback)</td>
    <td style="padding:15px">updateSipTrunkSecurityProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetSipTrunkSecurityProfile(body, callback)</td>
    <td style="padding:15px">getSipTrunkSecurityProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveSipTrunkSecurityProfile(body, callback)</td>
    <td style="padding:15px">removeSipTrunkSecurityProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListSipTrunkSecurityProfile(body, callback)</td>
    <td style="padding:15px">listSipTrunkSecurityProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postResetSipTrunkSecurityProfile(body, callback)</td>
    <td style="padding:15px">resetSipTrunkSecurityProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postApplySipTrunkSecurityProfile(body, callback)</td>
    <td style="padding:15px">applySipTrunkSecurityProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddTimePeriod(body, callback)</td>
    <td style="padding:15px">addTimePeriod</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateTimePeriod(body, callback)</td>
    <td style="padding:15px">updateTimePeriod</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetTimePeriod(body, callback)</td>
    <td style="padding:15px">getTimePeriod</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveTimePeriod(body, callback)</td>
    <td style="padding:15px">removeTimePeriod</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListTimePeriod(body, callback)</td>
    <td style="padding:15px">listTimePeriod</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddTimeSchedule(body, callback)</td>
    <td style="padding:15px">addTimeSchedule</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateTimeSchedule(body, callback)</td>
    <td style="padding:15px">updateTimeSchedule</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetTimeSchedule(body, callback)</td>
    <td style="padding:15px">getTimeSchedule</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveTimeSchedule(body, callback)</td>
    <td style="padding:15px">removeTimeSchedule</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListTimeSchedule(body, callback)</td>
    <td style="padding:15px">listTimeSchedule</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddTodAccess(body, callback)</td>
    <td style="padding:15px">addTodAccess</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateTodAccess(body, callback)</td>
    <td style="padding:15px">updateTodAccess</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetTodAccess(body, callback)</td>
    <td style="padding:15px">getTodAccess</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveTodAccess(body, callback)</td>
    <td style="padding:15px">removeTodAccess</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListTodAccess(body, callback)</td>
    <td style="padding:15px">listTodAccess</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddVoiceMailPilot(body, callback)</td>
    <td style="padding:15px">addVoiceMailPilot</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateVoiceMailPilot(body, callback)</td>
    <td style="padding:15px">updateVoiceMailPilot</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetVoiceMailPilot(body, callback)</td>
    <td style="padding:15px">getVoiceMailPilot</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveVoiceMailPilot(body, callback)</td>
    <td style="padding:15px">removeVoiceMailPilot</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListVoiceMailPilot(body, callback)</td>
    <td style="padding:15px">listVoiceMailPilot</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddProcessNode(body, callback)</td>
    <td style="padding:15px">addProcessNode</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateProcessNode(body, callback)</td>
    <td style="padding:15px">updateProcessNode</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetProcessNode(body, callback)</td>
    <td style="padding:15px">getProcessNode</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveProcessNode(body, callback)</td>
    <td style="padding:15px">removeProcessNode</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListProcessNode(body, callback)</td>
    <td style="padding:15px">listProcessNode</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddCallerFilterList(body, callback)</td>
    <td style="padding:15px">addCallerFilterList</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateCallerFilterList(body, callback)</td>
    <td style="padding:15px">updateCallerFilterList</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetCallerFilterList(body, callback)</td>
    <td style="padding:15px">getCallerFilterList</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveCallerFilterList(body, callback)</td>
    <td style="padding:15px">removeCallerFilterList</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListCallerFilterList(body, callback)</td>
    <td style="padding:15px">listCallerFilterList</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddRoutePartition(body, callback)</td>
    <td style="padding:15px">addRoutePartition</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateRoutePartition(body, callback)</td>
    <td style="padding:15px">updateRoutePartition</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetRoutePartition(body, callback)</td>
    <td style="padding:15px">getRoutePartition</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveRoutePartition(body, callback)</td>
    <td style="padding:15px">removeRoutePartition</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListRoutePartition(body, callback)</td>
    <td style="padding:15px">listRoutePartition</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestartRoutePartition(body, callback)</td>
    <td style="padding:15px">restartRoutePartition</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postApplyRoutePartition(body, callback)</td>
    <td style="padding:15px">applyRoutePartition</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddCss(body, callback)</td>
    <td style="padding:15px">addCss</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateCss(body, callback)</td>
    <td style="padding:15px">updateCss</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetCss(body, callback)</td>
    <td style="padding:15px">getCss</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveCss(body, callback)</td>
    <td style="padding:15px">removeCss</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListCss(body, callback)</td>
    <td style="padding:15px">listCss</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateCallManager(body, callback)</td>
    <td style="padding:15px">updateCallManager</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetCallManager(body, callback)</td>
    <td style="padding:15px">getCallManager</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListCallManager(body, callback)</td>
    <td style="padding:15px">listCallManager</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postResetCallManager(body, callback)</td>
    <td style="padding:15px">resetCallManager</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestartCallManager(body, callback)</td>
    <td style="padding:15px">restartCallManager</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postApplyCallManager(body, callback)</td>
    <td style="padding:15px">applyCallManager</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddMediaResourceGroup(body, callback)</td>
    <td style="padding:15px">addMediaResourceGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateMediaResourceGroup(body, callback)</td>
    <td style="padding:15px">updateMediaResourceGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetMediaResourceGroup(body, callback)</td>
    <td style="padding:15px">getMediaResourceGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveMediaResourceGroup(body, callback)</td>
    <td style="padding:15px">removeMediaResourceGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListMediaResourceGroup(body, callback)</td>
    <td style="padding:15px">listMediaResourceGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddMediaResourceList(body, callback)</td>
    <td style="padding:15px">addMediaResourceList</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateMediaResourceList(body, callback)</td>
    <td style="padding:15px">updateMediaResourceList</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetMediaResourceList(body, callback)</td>
    <td style="padding:15px">getMediaResourceList</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveMediaResourceList(body, callback)</td>
    <td style="padding:15px">removeMediaResourceList</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListMediaResourceList(body, callback)</td>
    <td style="padding:15px">listMediaResourceList</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddRegion(body, callback)</td>
    <td style="padding:15px">addRegion</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateRegion(body, callback)</td>
    <td style="padding:15px">updateRegion</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetRegion(body, callback)</td>
    <td style="padding:15px">getRegion</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveRegion(body, callback)</td>
    <td style="padding:15px">removeRegion</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListRegion(body, callback)</td>
    <td style="padding:15px">listRegion</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestartRegion(body, callback)</td>
    <td style="padding:15px">restartRegion</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postApplyRegion(body, callback)</td>
    <td style="padding:15px">applyRegion</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddAarGroup(body, callback)</td>
    <td style="padding:15px">addAarGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateAarGroup(body, callback)</td>
    <td style="padding:15px">updateAarGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetAarGroup(body, callback)</td>
    <td style="padding:15px">getAarGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveAarGroup(body, callback)</td>
    <td style="padding:15px">removeAarGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListAarGroup(body, callback)</td>
    <td style="padding:15px">listAarGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddPhysicalLocation(body, callback)</td>
    <td style="padding:15px">addPhysicalLocation</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdatePhysicalLocation(body, callback)</td>
    <td style="padding:15px">updatePhysicalLocation</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetPhysicalLocation(body, callback)</td>
    <td style="padding:15px">getPhysicalLocation</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemovePhysicalLocation(body, callback)</td>
    <td style="padding:15px">removePhysicalLocation</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListPhysicalLocation(body, callback)</td>
    <td style="padding:15px">listPhysicalLocation</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddRouteGroup(body, callback)</td>
    <td style="padding:15px">addRouteGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateRouteGroup(body, callback)</td>
    <td style="padding:15px">updateRouteGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetRouteGroup(body, callback)</td>
    <td style="padding:15px">getRouteGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveRouteGroup(body, callback)</td>
    <td style="padding:15px">removeRouteGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListRouteGroup(body, callback)</td>
    <td style="padding:15px">listRouteGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddDevicePool(body, callback)</td>
    <td style="padding:15px">addDevicePool</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateDevicePool(body, callback)</td>
    <td style="padding:15px">updateDevicePool</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetDevicePool(body, callback)</td>
    <td style="padding:15px">getDevicePool</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveDevicePool(body, callback)</td>
    <td style="padding:15px">removeDevicePool</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListDevicePool(body, callback)</td>
    <td style="padding:15px">listDevicePool</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postResetDevicePool(body, callback)</td>
    <td style="padding:15px">resetDevicePool</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestartDevicePool(body, callback)</td>
    <td style="padding:15px">restartDevicePool</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postApplyDevicePool(body, callback)</td>
    <td style="padding:15px">applyDevicePool</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddDeviceMobilityGroup(body, callback)</td>
    <td style="padding:15px">addDeviceMobilityGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateDeviceMobilityGroup(body, callback)</td>
    <td style="padding:15px">updateDeviceMobilityGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetDeviceMobilityGroup(body, callback)</td>
    <td style="padding:15px">getDeviceMobilityGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveDeviceMobilityGroup(body, callback)</td>
    <td style="padding:15px">removeDeviceMobilityGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListDeviceMobilityGroup(body, callback)</td>
    <td style="padding:15px">listDeviceMobilityGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddLocation(body, callback)</td>
    <td style="padding:15px">addLocation</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateLocation(body, callback)</td>
    <td style="padding:15px">updateLocation</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetLocation(body, callback)</td>
    <td style="padding:15px">getLocation</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveLocation(body, callback)</td>
    <td style="padding:15px">removeLocation</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListLocation(body, callback)</td>
    <td style="padding:15px">listLocation</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddSoftKeyTemplate(body, callback)</td>
    <td style="padding:15px">addSoftKeyTemplate</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateSoftKeyTemplate(body, callback)</td>
    <td style="padding:15px">updateSoftKeyTemplate</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetSoftKeyTemplate(body, callback)</td>
    <td style="padding:15px">getSoftKeyTemplate</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveSoftKeyTemplate(body, callback)</td>
    <td style="padding:15px">removeSoftKeyTemplate</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListSoftKeyTemplate(body, callback)</td>
    <td style="padding:15px">listSoftKeyTemplate</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestartSoftKeyTemplate(body, callback)</td>
    <td style="padding:15px">restartSoftKeyTemplate</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postApplySoftKeyTemplate(body, callback)</td>
    <td style="padding:15px">applySoftKeyTemplate</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddTranscoder(body, callback)</td>
    <td style="padding:15px">addTranscoder</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateTranscoder(body, callback)</td>
    <td style="padding:15px">updateTranscoder</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetTranscoder(body, callback)</td>
    <td style="padding:15px">getTranscoder</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveTranscoder(body, callback)</td>
    <td style="padding:15px">removeTranscoder</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListTranscoder(body, callback)</td>
    <td style="padding:15px">listTranscoder</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postResetTranscoder(body, callback)</td>
    <td style="padding:15px">resetTranscoder</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postApplyTranscoder(body, callback)</td>
    <td style="padding:15px">applyTranscoder</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddCommonDeviceConfig(body, callback)</td>
    <td style="padding:15px">addCommonDeviceConfig</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateCommonDeviceConfig(body, callback)</td>
    <td style="padding:15px">updateCommonDeviceConfig</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetCommonDeviceConfig(body, callback)</td>
    <td style="padding:15px">getCommonDeviceConfig</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveCommonDeviceConfig(body, callback)</td>
    <td style="padding:15px">removeCommonDeviceConfig</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListCommonDeviceConfig(body, callback)</td>
    <td style="padding:15px">listCommonDeviceConfig</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postResetCommonDeviceConfig(body, callback)</td>
    <td style="padding:15px">resetCommonDeviceConfig</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postApplyCommonDeviceConfig(body, callback)</td>
    <td style="padding:15px">applyCommonDeviceConfig</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddResourcePriorityNamespace(body, callback)</td>
    <td style="padding:15px">addResourcePriorityNamespace</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateResourcePriorityNamespace(body, callback)</td>
    <td style="padding:15px">updateResourcePriorityNamespace</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetResourcePriorityNamespace(body, callback)</td>
    <td style="padding:15px">getResourcePriorityNamespace</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveResourcePriorityNamespace(body, callback)</td>
    <td style="padding:15px">removeResourcePriorityNamespace</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListResourcePriorityNamespace(body, callback)</td>
    <td style="padding:15px">listResourcePriorityNamespace</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postResetResourcePriorityNamespace(body, callback)</td>
    <td style="padding:15px">resetResourcePriorityNamespace</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestartResourcePriorityNamespace(body, callback)</td>
    <td style="padding:15px">restartResourcePriorityNamespace</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postApplyResourcePriorityNamespace(body, callback)</td>
    <td style="padding:15px">applyResourcePriorityNamespace</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddResourcePriorityNamespaceList(body, callback)</td>
    <td style="padding:15px">addResourcePriorityNamespaceList</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateResourcePriorityNamespaceList(body, callback)</td>
    <td style="padding:15px">updateResourcePriorityNamespaceList</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetResourcePriorityNamespaceList(body, callback)</td>
    <td style="padding:15px">getResourcePriorityNamespaceList</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveResourcePriorityNamespaceList(body, callback)</td>
    <td style="padding:15px">removeResourcePriorityNamespaceList</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListResourcePriorityNamespaceList(body, callback)</td>
    <td style="padding:15px">listResourcePriorityNamespaceList</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postResetResourcePriorityNamespaceList(body, callback)</td>
    <td style="padding:15px">resetResourcePriorityNamespaceList</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestartResourcePriorityNamespaceList(body, callback)</td>
    <td style="padding:15px">restartResourcePriorityNamespaceList</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postApplyResourcePriorityNamespaceList(body, callback)</td>
    <td style="padding:15px">applyResourcePriorityNamespaceList</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddDeviceMobility(body, callback)</td>
    <td style="padding:15px">addDeviceMobility</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateDeviceMobility(body, callback)</td>
    <td style="padding:15px">updateDeviceMobility</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetDeviceMobility(body, callback)</td>
    <td style="padding:15px">getDeviceMobility</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveDeviceMobility(body, callback)</td>
    <td style="padding:15px">removeDeviceMobility</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListDeviceMobility(body, callback)</td>
    <td style="padding:15px">listDeviceMobility</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddCmcInfo(body, callback)</td>
    <td style="padding:15px">addCmcInfo</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateCmcInfo(body, callback)</td>
    <td style="padding:15px">updateCmcInfo</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetCmcInfo(body, callback)</td>
    <td style="padding:15px">getCmcInfo</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveCmcInfo(body, callback)</td>
    <td style="padding:15px">removeCmcInfo</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListCmcInfo(body, callback)</td>
    <td style="padding:15px">listCmcInfo</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddCredentialPolicy(body, callback)</td>
    <td style="padding:15px">addCredentialPolicy</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateCredentialPolicy(body, callback)</td>
    <td style="padding:15px">updateCredentialPolicy</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetCredentialPolicy(body, callback)</td>
    <td style="padding:15px">getCredentialPolicy</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveCredentialPolicy(body, callback)</td>
    <td style="padding:15px">removeCredentialPolicy</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListCredentialPolicy(body, callback)</td>
    <td style="padding:15px">listCredentialPolicy</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddFacInfo(body, callback)</td>
    <td style="padding:15px">addFacInfo</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateFacInfo(body, callback)</td>
    <td style="padding:15px">updateFacInfo</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetFacInfo(body, callback)</td>
    <td style="padding:15px">getFacInfo</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveFacInfo(body, callback)</td>
    <td style="padding:15px">removeFacInfo</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListFacInfo(body, callback)</td>
    <td style="padding:15px">listFacInfo</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddHuntList(body, callback)</td>
    <td style="padding:15px">addHuntList</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateHuntList(body, callback)</td>
    <td style="padding:15px">updateHuntList</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetHuntList(body, callback)</td>
    <td style="padding:15px">getHuntList</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveHuntList(body, callback)</td>
    <td style="padding:15px">removeHuntList</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListHuntList(body, callback)</td>
    <td style="padding:15px">listHuntList</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postResetHuntList(body, callback)</td>
    <td style="padding:15px">resetHuntList</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postApplyHuntList(body, callback)</td>
    <td style="padding:15px">applyHuntList</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddIvrUserLocale(body, callback)</td>
    <td style="padding:15px">addIvrUserLocale</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateIvrUserLocale(body, callback)</td>
    <td style="padding:15px">updateIvrUserLocale</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetIvrUserLocale(body, callback)</td>
    <td style="padding:15px">getIvrUserLocale</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveIvrUserLocale(body, callback)</td>
    <td style="padding:15px">removeIvrUserLocale</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListIvrUserLocale(body, callback)</td>
    <td style="padding:15px">listIvrUserLocale</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddLineGroup(body, callback)</td>
    <td style="padding:15px">addLineGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateLineGroup(body, callback)</td>
    <td style="padding:15px">updateLineGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetLineGroup(body, callback)</td>
    <td style="padding:15px">getLineGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveLineGroup(body, callback)</td>
    <td style="padding:15px">removeLineGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListLineGroup(body, callback)</td>
    <td style="padding:15px">listLineGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddRecordingProfile(body, callback)</td>
    <td style="padding:15px">addRecordingProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateRecordingProfile(body, callback)</td>
    <td style="padding:15px">updateRecordingProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetRecordingProfile(body, callback)</td>
    <td style="padding:15px">getRecordingProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveRecordingProfile(body, callback)</td>
    <td style="padding:15px">removeRecordingProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListRecordingProfile(body, callback)</td>
    <td style="padding:15px">listRecordingProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddRouteFilter(body, callback)</td>
    <td style="padding:15px">addRouteFilter</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateRouteFilter(body, callback)</td>
    <td style="padding:15px">updateRouteFilter</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetRouteFilter(body, callback)</td>
    <td style="padding:15px">getRouteFilter</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveRouteFilter(body, callback)</td>
    <td style="padding:15px">removeRouteFilter</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListRouteFilter(body, callback)</td>
    <td style="padding:15px">listRouteFilter</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddCallManagerGroup(body, callback)</td>
    <td style="padding:15px">addCallManagerGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateCallManagerGroup(body, callback)</td>
    <td style="padding:15px">updateCallManagerGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetCallManagerGroup(body, callback)</td>
    <td style="padding:15px">getCallManagerGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveCallManagerGroup(body, callback)</td>
    <td style="padding:15px">removeCallManagerGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListCallManagerGroup(body, callback)</td>
    <td style="padding:15px">listCallManagerGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postResetCallManagerGroup(body, callback)</td>
    <td style="padding:15px">resetCallManagerGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postApplyCallManagerGroup(body, callback)</td>
    <td style="padding:15px">applyCallManagerGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddUserGroup(body, callback)</td>
    <td style="padding:15px">addUserGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateUserGroup(body, callback)</td>
    <td style="padding:15px">updateUserGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetUserGroup(body, callback)</td>
    <td style="padding:15px">getUserGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveUserGroup(body, callback)</td>
    <td style="padding:15px">removeUserGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListUserGroup(body, callback)</td>
    <td style="padding:15px">listUserGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetDialPlan(body, callback)</td>
    <td style="padding:15px">getDialPlan</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListDialPlan(body, callback)</td>
    <td style="padding:15px">listDialPlan</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetDialPlanTag(body, callback)</td>
    <td style="padding:15px">getDialPlanTag</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListDialPlanTag(body, callback)</td>
    <td style="padding:15px">listDialPlanTag</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetDdi(body, callback)</td>
    <td style="padding:15px">getDdi</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListDdi(body, callback)</td>
    <td style="padding:15px">listDdi</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetMobileSmartClientProfile(body, callback)</td>
    <td style="padding:15px">getMobileSmartClientProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListMobileSmartClientProfile(body, callback)</td>
    <td style="padding:15px">listMobileSmartClientProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateProcessNodeService(body, callback)</td>
    <td style="padding:15px">updateProcessNodeService</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetProcessNodeService(body, callback)</td>
    <td style="padding:15px">getProcessNodeService</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListProcessNodeService(body, callback)</td>
    <td style="padding:15px">listProcessNodeService</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateMohAudioSource(body, callback)</td>
    <td style="padding:15px">updateMohAudioSource</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetMohAudioSource(body, callback)</td>
    <td style="padding:15px">getMohAudioSource</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveMohAudioSource(body, callback)</td>
    <td style="padding:15px">removeMohAudioSource</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListMohAudioSource(body, callback)</td>
    <td style="padding:15px">listMohAudioSource</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddDhcpServer(body, callback)</td>
    <td style="padding:15px">addDhcpServer</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateDhcpServer(body, callback)</td>
    <td style="padding:15px">updateDhcpServer</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetDhcpServer(body, callback)</td>
    <td style="padding:15px">getDhcpServer</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveDhcpServer(body, callback)</td>
    <td style="padding:15px">removeDhcpServer</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListDhcpServer(body, callback)</td>
    <td style="padding:15px">listDhcpServer</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddDhcpSubnet(body, callback)</td>
    <td style="padding:15px">addDhcpSubnet</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateDhcpSubnet(body, callback)</td>
    <td style="padding:15px">updateDhcpSubnet</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetDhcpSubnet(body, callback)</td>
    <td style="padding:15px">getDhcpSubnet</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveDhcpSubnet(body, callback)</td>
    <td style="padding:15px">removeDhcpSubnet</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListDhcpSubnet(body, callback)</td>
    <td style="padding:15px">listDhcpSubnet</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddCallPark(body, callback)</td>
    <td style="padding:15px">addCallPark</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateCallPark(body, callback)</td>
    <td style="padding:15px">updateCallPark</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetCallPark(body, callback)</td>
    <td style="padding:15px">getCallPark</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveCallPark(body, callback)</td>
    <td style="padding:15px">removeCallPark</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListCallPark(body, callback)</td>
    <td style="padding:15px">listCallPark</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddDirectedCallPark(body, callback)</td>
    <td style="padding:15px">addDirectedCallPark</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateDirectedCallPark(body, callback)</td>
    <td style="padding:15px">updateDirectedCallPark</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetDirectedCallPark(body, callback)</td>
    <td style="padding:15px">getDirectedCallPark</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveDirectedCallPark(body, callback)</td>
    <td style="padding:15px">removeDirectedCallPark</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListDirectedCallPark(body, callback)</td>
    <td style="padding:15px">listDirectedCallPark</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postResetDirectedCallPark(body, callback)</td>
    <td style="padding:15px">resetDirectedCallPark</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postApplyDirectedCallPark(body, callback)</td>
    <td style="padding:15px">applyDirectedCallPark</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddMeetMe(body, callback)</td>
    <td style="padding:15px">addMeetMe</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateMeetMe(body, callback)</td>
    <td style="padding:15px">updateMeetMe</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetMeetMe(body, callback)</td>
    <td style="padding:15px">getMeetMe</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveMeetMe(body, callback)</td>
    <td style="padding:15px">removeMeetMe</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListMeetMe(body, callback)</td>
    <td style="padding:15px">listMeetMe</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddConferenceNow(body, callback)</td>
    <td style="padding:15px">addConferenceNow</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateConferenceNow(body, callback)</td>
    <td style="padding:15px">updateConferenceNow</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetConferenceNow(body, callback)</td>
    <td style="padding:15px">getConferenceNow</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveConferenceNow(body, callback)</td>
    <td style="padding:15px">removeConferenceNow</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListConferenceNow(body, callback)</td>
    <td style="padding:15px">listConferenceNow</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddMobileVoiceAccess(body, callback)</td>
    <td style="padding:15px">addMobileVoiceAccess</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateMobileVoiceAccess(body, callback)</td>
    <td style="padding:15px">updateMobileVoiceAccess</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetMobileVoiceAccess(body, callback)</td>
    <td style="padding:15px">getMobileVoiceAccess</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveMobileVoiceAccess(body, callback)</td>
    <td style="padding:15px">removeMobileVoiceAccess</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddRouteList(body, callback)</td>
    <td style="padding:15px">addRouteList</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateRouteList(body, callback)</td>
    <td style="padding:15px">updateRouteList</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetRouteList(body, callback)</td>
    <td style="padding:15px">getRouteList</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveRouteList(body, callback)</td>
    <td style="padding:15px">removeRouteList</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListRouteList(body, callback)</td>
    <td style="padding:15px">listRouteList</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postResetRouteList(body, callback)</td>
    <td style="padding:15px">resetRouteList</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postApplyRouteList(body, callback)</td>
    <td style="padding:15px">applyRouteList</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddUser(body, callback)</td>
    <td style="padding:15px">addUser</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateUser(body, callback)</td>
    <td style="padding:15px">updateUser</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetUser(body, callback)</td>
    <td style="padding:15px">getUser</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveUser(body, callback)</td>
    <td style="padding:15px">removeUser</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListUser(body, callback)</td>
    <td style="padding:15px">listUser</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddAppUser(body, callback)</td>
    <td style="padding:15px">addAppUser</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateAppUser(body, callback)</td>
    <td style="padding:15px">updateAppUser</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetAppUser(body, callback)</td>
    <td style="padding:15px">getAppUser</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveAppUser(body, callback)</td>
    <td style="padding:15px">removeAppUser</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListAppUser(body, callback)</td>
    <td style="padding:15px">listAppUser</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddSipRealm(body, callback)</td>
    <td style="padding:15px">addSipRealm</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateSipRealm(body, callback)</td>
    <td style="padding:15px">updateSipRealm</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetSipRealm(body, callback)</td>
    <td style="padding:15px">getSipRealm</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveSipRealm(body, callback)</td>
    <td style="padding:15px">removeSipRealm</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListSipRealm(body, callback)</td>
    <td style="padding:15px">listSipRealm</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddPhoneNtp(body, callback)</td>
    <td style="padding:15px">addPhoneNtp</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdatePhoneNtp(body, callback)</td>
    <td style="padding:15px">updatePhoneNtp</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetPhoneNtp(body, callback)</td>
    <td style="padding:15px">getPhoneNtp</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemovePhoneNtp(body, callback)</td>
    <td style="padding:15px">removePhoneNtp</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListPhoneNtp(body, callback)</td>
    <td style="padding:15px">listPhoneNtp</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddDateTimeGroup(body, callback)</td>
    <td style="padding:15px">addDateTimeGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateDateTimeGroup(body, callback)</td>
    <td style="padding:15px">updateDateTimeGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetDateTimeGroup(body, callback)</td>
    <td style="padding:15px">getDateTimeGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveDateTimeGroup(body, callback)</td>
    <td style="padding:15px">removeDateTimeGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListDateTimeGroup(body, callback)</td>
    <td style="padding:15px">listDateTimeGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postResetDateTimeGroup(body, callback)</td>
    <td style="padding:15px">resetDateTimeGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postApplyDateTimeGroup(body, callback)</td>
    <td style="padding:15px">applyDateTimeGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddPresenceGroup(body, callback)</td>
    <td style="padding:15px">addPresenceGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdatePresenceGroup(body, callback)</td>
    <td style="padding:15px">updatePresenceGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetPresenceGroup(body, callback)</td>
    <td style="padding:15px">getPresenceGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemovePresenceGroup(body, callback)</td>
    <td style="padding:15px">removePresenceGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListPresenceGroup(body, callback)</td>
    <td style="padding:15px">listPresenceGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddGeoLocation(body, callback)</td>
    <td style="padding:15px">addGeoLocation</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateGeoLocation(body, callback)</td>
    <td style="padding:15px">updateGeoLocation</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetGeoLocation(body, callback)</td>
    <td style="padding:15px">getGeoLocation</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveGeoLocation(body, callback)</td>
    <td style="padding:15px">removeGeoLocation</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListGeoLocation(body, callback)</td>
    <td style="padding:15px">listGeoLocation</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddSrst(body, callback)</td>
    <td style="padding:15px">addSrst</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateSrst(body, callback)</td>
    <td style="padding:15px">updateSrst</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetSrst(body, callback)</td>
    <td style="padding:15px">getSrst</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveSrst(body, callback)</td>
    <td style="padding:15px">removeSrst</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListSrst(body, callback)</td>
    <td style="padding:15px">listSrst</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postResetSrst(body, callback)</td>
    <td style="padding:15px">resetSrst</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestartSrst(body, callback)</td>
    <td style="padding:15px">restartSrst</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postApplySrst(body, callback)</td>
    <td style="padding:15px">applySrst</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddMlppDomain(body, callback)</td>
    <td style="padding:15px">addMlppDomain</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateMlppDomain(body, callback)</td>
    <td style="padding:15px">updateMlppDomain</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetMlppDomain(body, callback)</td>
    <td style="padding:15px">getMlppDomain</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveMlppDomain(body, callback)</td>
    <td style="padding:15px">removeMlppDomain</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListMlppDomain(body, callback)</td>
    <td style="padding:15px">listMlppDomain</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddCumaServerSecurityProfile(body, callback)</td>
    <td style="padding:15px">addCumaServerSecurityProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateCumaServerSecurityProfile(body, callback)</td>
    <td style="padding:15px">updateCumaServerSecurityProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetCumaServerSecurityProfile(body, callback)</td>
    <td style="padding:15px">getCumaServerSecurityProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveCumaServerSecurityProfile(body, callback)</td>
    <td style="padding:15px">removeCumaServerSecurityProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListCumaServerSecurityProfile(body, callback)</td>
    <td style="padding:15px">listCumaServerSecurityProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddApplicationServer(body, callback)</td>
    <td style="padding:15px">addApplicationServer</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateApplicationServer(body, callback)</td>
    <td style="padding:15px">updateApplicationServer</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetApplicationServer(body, callback)</td>
    <td style="padding:15px">getApplicationServer</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveApplicationServer(body, callback)</td>
    <td style="padding:15px">removeApplicationServer</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListApplicationServer(body, callback)</td>
    <td style="padding:15px">listApplicationServer</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddApplicationUserCapfProfile(body, callback)</td>
    <td style="padding:15px">addApplicationUserCapfProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateApplicationUserCapfProfile(body, callback)</td>
    <td style="padding:15px">updateApplicationUserCapfProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetApplicationUserCapfProfile(body, callback)</td>
    <td style="padding:15px">getApplicationUserCapfProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveApplicationUserCapfProfile(body, callback)</td>
    <td style="padding:15px">removeApplicationUserCapfProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListApplicationUserCapfProfile(body, callback)</td>
    <td style="padding:15px">listApplicationUserCapfProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddEndUserCapfProfile(body, callback)</td>
    <td style="padding:15px">addEndUserCapfProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateEndUserCapfProfile(body, callback)</td>
    <td style="padding:15px">updateEndUserCapfProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetEndUserCapfProfile(body, callback)</td>
    <td style="padding:15px">getEndUserCapfProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveEndUserCapfProfile(body, callback)</td>
    <td style="padding:15px">removeEndUserCapfProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListEndUserCapfProfile(body, callback)</td>
    <td style="padding:15px">listEndUserCapfProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateServiceParameter(body, callback)</td>
    <td style="padding:15px">updateServiceParameter</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetServiceParameter(body, callback)</td>
    <td style="padding:15px">getServiceParameter</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListServiceParameter(body, callback)</td>
    <td style="padding:15px">listServiceParameter</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddUserPhoneAssociation(body, callback)</td>
    <td style="padding:15px">addUserPhoneAssociation</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddGeoLocationFilter(body, callback)</td>
    <td style="padding:15px">addGeoLocationFilter</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateGeoLocationFilter(body, callback)</td>
    <td style="padding:15px">updateGeoLocationFilter</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetGeoLocationFilter(body, callback)</td>
    <td style="padding:15px">getGeoLocationFilter</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveGeoLocationFilter(body, callback)</td>
    <td style="padding:15px">removeGeoLocationFilter</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListGeoLocationFilter(body, callback)</td>
    <td style="padding:15px">listGeoLocationFilter</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddVoiceMailProfile(body, callback)</td>
    <td style="padding:15px">addVoiceMailProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateVoiceMailProfile(body, callback)</td>
    <td style="padding:15px">updateVoiceMailProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetVoiceMailProfile(body, callback)</td>
    <td style="padding:15px">getVoiceMailProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveVoiceMailProfile(body, callback)</td>
    <td style="padding:15px">removeVoiceMailProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListVoiceMailProfile(body, callback)</td>
    <td style="padding:15px">listVoiceMailProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postResetVoiceMailProfile(body, callback)</td>
    <td style="padding:15px">resetVoiceMailProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestartVoiceMailProfile(body, callback)</td>
    <td style="padding:15px">restartVoiceMailProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postApplyVoiceMailProfile(body, callback)</td>
    <td style="padding:15px">applyVoiceMailProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddVoiceMailPort(body, callback)</td>
    <td style="padding:15px">addVoiceMailPort</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateVoiceMailPort(body, callback)</td>
    <td style="padding:15px">updateVoiceMailPort</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetVoiceMailPort(body, callback)</td>
    <td style="padding:15px">getVoiceMailPort</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveVoiceMailPort(body, callback)</td>
    <td style="padding:15px">removeVoiceMailPort</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListVoiceMailPort(body, callback)</td>
    <td style="padding:15px">listVoiceMailPort</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postResetVoiceMailPort(body, callback)</td>
    <td style="padding:15px">resetVoiceMailPort</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestartVoiceMailPort(body, callback)</td>
    <td style="padding:15px">restartVoiceMailPort</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postApplyVoiceMailPort(body, callback)</td>
    <td style="padding:15px">applyVoiceMailPort</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddGatekeeper(body, callback)</td>
    <td style="padding:15px">addGatekeeper</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateGatekeeper(body, callback)</td>
    <td style="padding:15px">updateGatekeeper</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetGatekeeper(body, callback)</td>
    <td style="padding:15px">getGatekeeper</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveGatekeeper(body, callback)</td>
    <td style="padding:15px">removeGatekeeper</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListGatekeeper(body, callback)</td>
    <td style="padding:15px">listGatekeeper</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postResetGatekeeper(body, callback)</td>
    <td style="padding:15px">resetGatekeeper</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestartGatekeeper(body, callback)</td>
    <td style="padding:15px">restartGatekeeper</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postApplyGatekeeper(body, callback)</td>
    <td style="padding:15px">applyGatekeeper</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddPhoneButtonTemplate(body, callback)</td>
    <td style="padding:15px">addPhoneButtonTemplate</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdatePhoneButtonTemplate(body, callback)</td>
    <td style="padding:15px">updatePhoneButtonTemplate</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetPhoneButtonTemplate(body, callback)</td>
    <td style="padding:15px">getPhoneButtonTemplate</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemovePhoneButtonTemplate(body, callback)</td>
    <td style="padding:15px">removePhoneButtonTemplate</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListPhoneButtonTemplate(body, callback)</td>
    <td style="padding:15px">listPhoneButtonTemplate</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestartPhoneButtonTemplate(body, callback)</td>
    <td style="padding:15px">restartPhoneButtonTemplate</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postApplyPhoneButtonTemplate(body, callback)</td>
    <td style="padding:15px">applyPhoneButtonTemplate</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddCommonPhoneConfig(body, callback)</td>
    <td style="padding:15px">addCommonPhoneConfig</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateCommonPhoneConfig(body, callback)</td>
    <td style="padding:15px">updateCommonPhoneConfig</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetCommonPhoneConfig(body, callback)</td>
    <td style="padding:15px">getCommonPhoneConfig</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveCommonPhoneConfig(body, callback)</td>
    <td style="padding:15px">removeCommonPhoneConfig</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListCommonPhoneConfig(body, callback)</td>
    <td style="padding:15px">listCommonPhoneConfig</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postResetCommonPhoneConfig(body, callback)</td>
    <td style="padding:15px">resetCommonPhoneConfig</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postApplyCommonPhoneConfig(body, callback)</td>
    <td style="padding:15px">applyCommonPhoneConfig</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddMessageWaiting(body, callback)</td>
    <td style="padding:15px">addMessageWaiting</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateMessageWaiting(body, callback)</td>
    <td style="padding:15px">updateMessageWaiting</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetMessageWaiting(body, callback)</td>
    <td style="padding:15px">getMessageWaiting</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveMessageWaiting(body, callback)</td>
    <td style="padding:15px">removeMessageWaiting</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListMessageWaiting(body, callback)</td>
    <td style="padding:15px">listMessageWaiting</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddIpPhoneServices(body, callback)</td>
    <td style="padding:15px">addIpPhoneServices</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateIpPhoneServices(body, callback)</td>
    <td style="padding:15px">updateIpPhoneServices</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetIpPhoneServices(body, callback)</td>
    <td style="padding:15px">getIpPhoneServices</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveIpPhoneServices(body, callback)</td>
    <td style="padding:15px">removeIpPhoneServices</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListIpPhoneServices(body, callback)</td>
    <td style="padding:15px">listIpPhoneServices</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddCtiRoutePoint(body, callback)</td>
    <td style="padding:15px">addCtiRoutePoint</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateCtiRoutePoint(body, callback)</td>
    <td style="padding:15px">updateCtiRoutePoint</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetCtiRoutePoint(body, callback)</td>
    <td style="padding:15px">getCtiRoutePoint</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveCtiRoutePoint(body, callback)</td>
    <td style="padding:15px">removeCtiRoutePoint</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListCtiRoutePoint(body, callback)</td>
    <td style="padding:15px">listCtiRoutePoint</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postResetCtiRoutePoint(body, callback)</td>
    <td style="padding:15px">resetCtiRoutePoint</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestartCtiRoutePoint(body, callback)</td>
    <td style="padding:15px">restartCtiRoutePoint</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postApplyCtiRoutePoint(body, callback)</td>
    <td style="padding:15px">applyCtiRoutePoint</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddTransPattern(body, callback)</td>
    <td style="padding:15px">addTransPattern</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateTransPattern(body, callback)</td>
    <td style="padding:15px">updateTransPattern</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetTransPattern(body, callback)</td>
    <td style="padding:15px">getTransPattern</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveTransPattern(body, callback)</td>
    <td style="padding:15px">removeTransPattern</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListTransPattern(body, callback)</td>
    <td style="padding:15px">listTransPattern</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetTransPatternOptions(body, callback)</td>
    <td style="padding:15px">getTransPatternOptions</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddCallingPartyTransformationPattern(body, callback)</td>
    <td style="padding:15px">addCallingPartyTransformationPattern</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateCallingPartyTransformationPattern(body, callback)</td>
    <td style="padding:15px">updateCallingPartyTransformationPattern</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetCallingPartyTransformationPattern(body, callback)</td>
    <td style="padding:15px">getCallingPartyTransformationPattern</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveCallingPartyTransformationPattern(body, callback)</td>
    <td style="padding:15px">removeCallingPartyTransformationPattern</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListCallingPartyTransformationPattern(body, callback)</td>
    <td style="padding:15px">listCallingPartyTransformationPattern</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddSipRoutePattern(body, callback)</td>
    <td style="padding:15px">addSipRoutePattern</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateSipRoutePattern(body, callback)</td>
    <td style="padding:15px">updateSipRoutePattern</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetSipRoutePattern(body, callback)</td>
    <td style="padding:15px">getSipRoutePattern</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveSipRoutePattern(body, callback)</td>
    <td style="padding:15px">removeSipRoutePattern</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListSipRoutePattern(body, callback)</td>
    <td style="padding:15px">listSipRoutePattern</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddHuntPilot(body, callback)</td>
    <td style="padding:15px">addHuntPilot</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateHuntPilot(body, callback)</td>
    <td style="padding:15px">updateHuntPilot</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetHuntPilot(body, callback)</td>
    <td style="padding:15px">getHuntPilot</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveHuntPilot(body, callback)</td>
    <td style="padding:15px">removeHuntPilot</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListHuntPilot(body, callback)</td>
    <td style="padding:15px">listHuntPilot</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddRoutePattern(body, callback)</td>
    <td style="padding:15px">addRoutePattern</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateRoutePattern(body, callback)</td>
    <td style="padding:15px">updateRoutePattern</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetRoutePattern(body, callback)</td>
    <td style="padding:15px">getRoutePattern</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveRoutePattern(body, callback)</td>
    <td style="padding:15px">removeRoutePattern</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListRoutePattern(body, callback)</td>
    <td style="padding:15px">listRoutePattern</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddApplicationDialRules(body, callback)</td>
    <td style="padding:15px">addApplicationDialRules</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateApplicationDialRules(body, callback)</td>
    <td style="padding:15px">updateApplicationDialRules</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetApplicationDialRules(body, callback)</td>
    <td style="padding:15px">getApplicationDialRules</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveApplicationDialRules(body, callback)</td>
    <td style="padding:15px">removeApplicationDialRules</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListApplicationDialRules(body, callback)</td>
    <td style="padding:15px">listApplicationDialRules</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddDirectoryLookupDialRules(body, callback)</td>
    <td style="padding:15px">addDirectoryLookupDialRules</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateDirectoryLookupDialRules(body, callback)</td>
    <td style="padding:15px">updateDirectoryLookupDialRules</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetDirectoryLookupDialRules(body, callback)</td>
    <td style="padding:15px">getDirectoryLookupDialRules</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveDirectoryLookupDialRules(body, callback)</td>
    <td style="padding:15px">removeDirectoryLookupDialRules</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListDirectoryLookupDialRules(body, callback)</td>
    <td style="padding:15px">listDirectoryLookupDialRules</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddPhoneSecurityProfile(body, callback)</td>
    <td style="padding:15px">addPhoneSecurityProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdatePhoneSecurityProfile(body, callback)</td>
    <td style="padding:15px">updatePhoneSecurityProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetPhoneSecurityProfile(body, callback)</td>
    <td style="padding:15px">getPhoneSecurityProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemovePhoneSecurityProfile(body, callback)</td>
    <td style="padding:15px">removePhoneSecurityProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListPhoneSecurityProfile(body, callback)</td>
    <td style="padding:15px">listPhoneSecurityProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postResetPhoneSecurityProfile(body, callback)</td>
    <td style="padding:15px">resetPhoneSecurityProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postApplyPhoneSecurityProfile(body, callback)</td>
    <td style="padding:15px">applyPhoneSecurityProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddSipDialRules(body, callback)</td>
    <td style="padding:15px">addSipDialRules</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateSipDialRules(body, callback)</td>
    <td style="padding:15px">updateSipDialRules</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetSipDialRules(body, callback)</td>
    <td style="padding:15px">getSipDialRules</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveSipDialRules(body, callback)</td>
    <td style="padding:15px">removeSipDialRules</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListSipDialRules(body, callback)</td>
    <td style="padding:15px">listSipDialRules</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddConferenceBridge(body, callback)</td>
    <td style="padding:15px">addConferenceBridge</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateConferenceBridge(body, callback)</td>
    <td style="padding:15px">updateConferenceBridge</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetConferenceBridge(body, callback)</td>
    <td style="padding:15px">getConferenceBridge</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveConferenceBridge(body, callback)</td>
    <td style="padding:15px">removeConferenceBridge</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListConferenceBridge(body, callback)</td>
    <td style="padding:15px">listConferenceBridge</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postResetConferenceBridge(body, callback)</td>
    <td style="padding:15px">resetConferenceBridge</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestartConferenceBridge(body, callback)</td>
    <td style="padding:15px">restartConferenceBridge</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postApplyConferenceBridge(body, callback)</td>
    <td style="padding:15px">applyConferenceBridge</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateAnnunciator(body, callback)</td>
    <td style="padding:15px">updateAnnunciator</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetAnnunciator(body, callback)</td>
    <td style="padding:15px">getAnnunciator</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListAnnunciator(body, callback)</td>
    <td style="padding:15px">listAnnunciator</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateInteractiveVoiceResponse(body, callback)</td>
    <td style="padding:15px">updateInteractiveVoiceResponse</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetInteractiveVoiceResponse(body, callback)</td>
    <td style="padding:15px">getInteractiveVoiceResponse</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListInteractiveVoiceResponse(body, callback)</td>
    <td style="padding:15px">listInteractiveVoiceResponse</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddMtp(body, callback)</td>
    <td style="padding:15px">addMtp</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateMtp(body, callback)</td>
    <td style="padding:15px">updateMtp</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetMtp(body, callback)</td>
    <td style="padding:15px">getMtp</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveMtp(body, callback)</td>
    <td style="padding:15px">removeMtp</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListMtp(body, callback)</td>
    <td style="padding:15px">listMtp</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postResetMtp(body, callback)</td>
    <td style="padding:15px">resetMtp</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestartMtp(body, callback)</td>
    <td style="padding:15px">restartMtp</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postApplyMtp(body, callback)</td>
    <td style="padding:15px">applyMtp</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateFixedMohAudioSource(body, callback)</td>
    <td style="padding:15px">updateFixedMohAudioSource</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetFixedMohAudioSource(body, callback)</td>
    <td style="padding:15px">getFixedMohAudioSource</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateAarGroupMatrix(body, callback)</td>
    <td style="padding:15px">updateAarGroupMatrix</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddRemoteDestinationProfile(body, callback)</td>
    <td style="padding:15px">addRemoteDestinationProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateRemoteDestinationProfile(body, callback)</td>
    <td style="padding:15px">updateRemoteDestinationProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetRemoteDestinationProfile(body, callback)</td>
    <td style="padding:15px">getRemoteDestinationProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveRemoteDestinationProfile(body, callback)</td>
    <td style="padding:15px">removeRemoteDestinationProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListRemoteDestinationProfile(body, callback)</td>
    <td style="padding:15px">listRemoteDestinationProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddLine(body, callback)</td>
    <td style="padding:15px">addLine</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateLine(body, callback)</td>
    <td style="padding:15px">updateLine</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetLine(body, callback)</td>
    <td style="padding:15px">getLine</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveLine(body, callback)</td>
    <td style="padding:15px">removeLine</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListLine(body, callback)</td>
    <td style="padding:15px">listLine</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postResetLine(body, callback)</td>
    <td style="padding:15px">resetLine</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestartLine(body, callback)</td>
    <td style="padding:15px">restartLine</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postApplyLine(body, callback)</td>
    <td style="padding:15px">applyLine</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetLineOptions(body, callback)</td>
    <td style="padding:15px">getLineOptions</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddDefaultDeviceProfile(body, callback)</td>
    <td style="padding:15px">addDefaultDeviceProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateDefaultDeviceProfile(body, callback)</td>
    <td style="padding:15px">updateDefaultDeviceProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetDefaultDeviceProfile(body, callback)</td>
    <td style="padding:15px">getDefaultDeviceProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveDefaultDeviceProfile(body, callback)</td>
    <td style="padding:15px">removeDefaultDeviceProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListDefaultDeviceProfile(body, callback)</td>
    <td style="padding:15px">listDefaultDeviceProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddH323Phone(body, callback)</td>
    <td style="padding:15px">addH323Phone</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateH323Phone(body, callback)</td>
    <td style="padding:15px">updateH323Phone</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetH323Phone(body, callback)</td>
    <td style="padding:15px">getH323Phone</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveH323Phone(body, callback)</td>
    <td style="padding:15px">removeH323Phone</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListH323Phone(body, callback)</td>
    <td style="padding:15px">listH323Phone</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postResetH323Phone(body, callback)</td>
    <td style="padding:15px">resetH323Phone</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestartH323Phone(body, callback)</td>
    <td style="padding:15px">restartH323Phone</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postApplyH323Phone(body, callback)</td>
    <td style="padding:15px">applyH323Phone</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateMohServer(body, callback)</td>
    <td style="padding:15px">updateMohServer</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetMohServer(body, callback)</td>
    <td style="padding:15px">getMohServer</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListMohServer(body, callback)</td>
    <td style="padding:15px">listMohServer</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddH323Trunk(body, callback)</td>
    <td style="padding:15px">addH323Trunk</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateH323Trunk(body, callback)</td>
    <td style="padding:15px">updateH323Trunk</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetH323Trunk(body, callback)</td>
    <td style="padding:15px">getH323Trunk</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveH323Trunk(body, callback)</td>
    <td style="padding:15px">removeH323Trunk</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListH323Trunk(body, callback)</td>
    <td style="padding:15px">listH323Trunk</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postResetH323Trunk(body, callback)</td>
    <td style="padding:15px">resetH323Trunk</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestartH323Trunk(body, callback)</td>
    <td style="padding:15px">restartH323Trunk</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddPhone(body, callback)</td>
    <td style="padding:15px">addPhone</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdatePhone(body, callback)</td>
    <td style="padding:15px">updatePhone</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetPhone(body, callback)</td>
    <td style="padding:15px">getPhone</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemovePhone(body, callback)</td>
    <td style="padding:15px">removePhone</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListPhone(body, callback)</td>
    <td style="padding:15px">listPhone</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postResetPhone(body, callback)</td>
    <td style="padding:15px">resetPhone</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestartPhone(body, callback)</td>
    <td style="padding:15px">restartPhone</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postApplyPhone(body, callback)</td>
    <td style="padding:15px">applyPhone</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWipePhone(body, callback)</td>
    <td style="padding:15px">wipePhone</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postLockPhone(body, callback)</td>
    <td style="padding:15px">lockPhone</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetPhoneOptions(body, callback)</td>
    <td style="padding:15px">getPhoneOptions</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddH323Gateway(body, callback)</td>
    <td style="padding:15px">addH323Gateway</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateH323Gateway(body, callback)</td>
    <td style="padding:15px">updateH323Gateway</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetH323Gateway(body, callback)</td>
    <td style="padding:15px">getH323Gateway</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveH323Gateway(body, callback)</td>
    <td style="padding:15px">removeH323Gateway</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListH323Gateway(body, callback)</td>
    <td style="padding:15px">listH323Gateway</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postResetH323Gateway(body, callback)</td>
    <td style="padding:15px">resetH323Gateway</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestartH323Gateway(body, callback)</td>
    <td style="padding:15px">restartH323Gateway</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postApplyH323Gateway(body, callback)</td>
    <td style="padding:15px">applyH323Gateway</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddDeviceProfile(body, callback)</td>
    <td style="padding:15px">addDeviceProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateDeviceProfile(body, callback)</td>
    <td style="padding:15px">updateDeviceProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetDeviceProfile(body, callback)</td>
    <td style="padding:15px">getDeviceProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveDeviceProfile(body, callback)</td>
    <td style="padding:15px">removeDeviceProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListDeviceProfile(body, callback)</td>
    <td style="padding:15px">listDeviceProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetDeviceProfileOptions(body, callback)</td>
    <td style="padding:15px">getDeviceProfileOptions</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddRemoteDestination(body, callback)</td>
    <td style="padding:15px">addRemoteDestination</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateRemoteDestination(body, callback)</td>
    <td style="padding:15px">updateRemoteDestination</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetRemoteDestination(body, callback)</td>
    <td style="padding:15px">getRemoteDestination</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveRemoteDestination(body, callback)</td>
    <td style="padding:15px">removeRemoteDestination</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListRemoteDestination(body, callback)</td>
    <td style="padding:15px">listRemoteDestination</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddVg224(body, callback)</td>
    <td style="padding:15px">addVg224</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateVg224(body, callback)</td>
    <td style="padding:15px">updateVg224</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetVg224(body, callback)</td>
    <td style="padding:15px">getVg224</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveVg224(body, callback)</td>
    <td style="padding:15px">removeVg224</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddGateway(body, callback)</td>
    <td style="padding:15px">addGateway</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateGateway(body, callback)</td>
    <td style="padding:15px">updateGateway</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetGateway(body, callback)</td>
    <td style="padding:15px">getGateway</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveGateway(body, callback)</td>
    <td style="padding:15px">removeGateway</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListGateway(body, callback)</td>
    <td style="padding:15px">listGateway</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postResetGateway(body, callback)</td>
    <td style="padding:15px">resetGateway</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postApplyGateway(body, callback)</td>
    <td style="padding:15px">applyGateway</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddGatewayEndpointAnalogAccess(body, callback)</td>
    <td style="padding:15px">addGatewayEndpointAnalogAccess</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateGatewayEndpointAnalogAccess(body, callback)</td>
    <td style="padding:15px">updateGatewayEndpointAnalogAccess</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetGatewayEndpointAnalogAccess(body, callback)</td>
    <td style="padding:15px">getGatewayEndpointAnalogAccess</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveGatewayEndpointAnalogAccess(body, callback)</td>
    <td style="padding:15px">removeGatewayEndpointAnalogAccess</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddGatewayEndpointDigitalAccessPri(body, callback)</td>
    <td style="padding:15px">addGatewayEndpointDigitalAccessPri</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateGatewayEndpointDigitalAccessPri(body, callback)</td>
    <td style="padding:15px">updateGatewayEndpointDigitalAccessPri</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetGatewayEndpointDigitalAccessPri(body, callback)</td>
    <td style="padding:15px">getGatewayEndpointDigitalAccessPri</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveGatewayEndpointDigitalAccessPri(body, callback)</td>
    <td style="padding:15px">removeGatewayEndpointDigitalAccessPri</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddGatewayEndpointDigitalAccessBri(body, callback)</td>
    <td style="padding:15px">addGatewayEndpointDigitalAccessBri</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateGatewayEndpointDigitalAccessBri(body, callback)</td>
    <td style="padding:15px">updateGatewayEndpointDigitalAccessBri</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetGatewayEndpointDigitalAccessBri(body, callback)</td>
    <td style="padding:15px">getGatewayEndpointDigitalAccessBri</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveGatewayEndpointDigitalAccessBri(body, callback)</td>
    <td style="padding:15px">removeGatewayEndpointDigitalAccessBri</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddGatewayEndpointDigitalAccessT1(body, callback)</td>
    <td style="padding:15px">addGatewayEndpointDigitalAccessT1</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateGatewayEndpointDigitalAccessT1(body, callback)</td>
    <td style="padding:15px">updateGatewayEndpointDigitalAccessT1</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetGatewayEndpointDigitalAccessT1(body, callback)</td>
    <td style="padding:15px">getGatewayEndpointDigitalAccessT1</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveGatewayEndpointDigitalAccessT1(body, callback)</td>
    <td style="padding:15px">removeGatewayEndpointDigitalAccessT1</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddCiscoCatalyst600024PortFXSGateway(body, callback)</td>
    <td style="padding:15px">addCiscoCatalyst600024PortFXSGateway</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateCiscoCatalyst600024PortFXSGateway(body, callback)</td>
    <td style="padding:15px">updateCiscoCatalyst600024PortFXSGateway</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetCiscoCatalyst600024PortFXSGateway(body, callback)</td>
    <td style="padding:15px">getCiscoCatalyst600024PortFXSGateway</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveCiscoCatalyst600024PortFXSGateway(body, callback)</td>
    <td style="padding:15px">removeCiscoCatalyst600024PortFXSGateway</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListCiscoCatalyst600024PortFXSGateway(body, callback)</td>
    <td style="padding:15px">listCiscoCatalyst600024PortFXSGateway</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postResetCiscoCatalyst600024PortFXSGateway(body, callback)</td>
    <td style="padding:15px">resetCiscoCatalyst600024PortFXSGateway</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestartCiscoCatalyst600024PortFXSGateway(body, callback)</td>
    <td style="padding:15px">restartCiscoCatalyst600024PortFXSGateway</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postApplyCiscoCatalyst600024PortFXSGateway(body, callback)</td>
    <td style="padding:15px">applyCiscoCatalyst600024PortFXSGateway</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddCiscoCatalyst6000E1VoIPGateway(body, callback)</td>
    <td style="padding:15px">addCiscoCatalyst6000E1VoIPGateway</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateCiscoCatalyst6000E1VoIPGateway(body, callback)</td>
    <td style="padding:15px">updateCiscoCatalyst6000E1VoIPGateway</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetCiscoCatalyst6000E1VoIPGateway(body, callback)</td>
    <td style="padding:15px">getCiscoCatalyst6000E1VoIPGateway</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveCiscoCatalyst6000E1VoIPGateway(body, callback)</td>
    <td style="padding:15px">removeCiscoCatalyst6000E1VoIPGateway</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListCiscoCatalyst6000E1VoIPGateway(body, callback)</td>
    <td style="padding:15px">listCiscoCatalyst6000E1VoIPGateway</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postResetCiscoCatalyst6000E1VoIPGateway(body, callback)</td>
    <td style="padding:15px">resetCiscoCatalyst6000E1VoIPGateway</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestartCiscoCatalyst6000E1VoIPGateway(body, callback)</td>
    <td style="padding:15px">restartCiscoCatalyst6000E1VoIPGateway</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postApplyCiscoCatalyst6000E1VoIPGateway(body, callback)</td>
    <td style="padding:15px">applyCiscoCatalyst6000E1VoIPGateway</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddCiscoCatalyst6000T1VoIPGatewayPri(body, callback)</td>
    <td style="padding:15px">addCiscoCatalyst6000T1VoIPGatewayPri</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateCiscoCatalyst6000T1VoIPGatewayPri(body, callback)</td>
    <td style="padding:15px">updateCiscoCatalyst6000T1VoIPGatewayPri</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetCiscoCatalyst6000T1VoIPGatewayPri(body, callback)</td>
    <td style="padding:15px">getCiscoCatalyst6000T1VoIPGatewayPri</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveCiscoCatalyst6000T1VoIPGatewayPri(body, callback)</td>
    <td style="padding:15px">removeCiscoCatalyst6000T1VoIPGatewayPri</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListCiscoCatalyst6000T1VoIPGatewayPri(body, callback)</td>
    <td style="padding:15px">listCiscoCatalyst6000T1VoIPGatewayPri</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postResetCiscoCatalyst6000T1VoIPGatewayPri(body, callback)</td>
    <td style="padding:15px">resetCiscoCatalyst6000T1VoIPGatewayPri</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestartCiscoCatalyst6000T1VoIPGatewayPri(body, callback)</td>
    <td style="padding:15px">restartCiscoCatalyst6000T1VoIPGatewayPri</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postApplyCiscoCatalyst6000T1VoIPGatewayPri(body, callback)</td>
    <td style="padding:15px">applyCiscoCatalyst6000T1VoIPGatewayPri</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddCiscoCatalyst6000T1VoIPGatewayT1(body, callback)</td>
    <td style="padding:15px">addCiscoCatalyst6000T1VoIPGatewayT1</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateCiscoCatalyst6000T1VoIPGatewayT1(body, callback)</td>
    <td style="padding:15px">updateCiscoCatalyst6000T1VoIPGatewayT1</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetCiscoCatalyst6000T1VoIPGatewayT1(body, callback)</td>
    <td style="padding:15px">getCiscoCatalyst6000T1VoIPGatewayT1</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveCiscoCatalyst6000T1VoIPGatewayT1(body, callback)</td>
    <td style="padding:15px">removeCiscoCatalyst6000T1VoIPGatewayT1</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListCiscoCatalyst6000T1VoIPGatewayT1(body, callback)</td>
    <td style="padding:15px">listCiscoCatalyst6000T1VoIPGatewayT1</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postResetCiscoCatalyst6000T1VoIPGatewayT1(body, callback)</td>
    <td style="padding:15px">resetCiscoCatalyst6000T1VoIPGatewayT1</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestartCiscoCatalyst6000T1VoIPGatewayT1(body, callback)</td>
    <td style="padding:15px">restartCiscoCatalyst6000T1VoIPGatewayT1</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postApplyCiscoCatalyst6000T1VoIPGatewayT1(body, callback)</td>
    <td style="padding:15px">applyCiscoCatalyst6000T1VoIPGatewayT1</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddCallPickupGroup(body, callback)</td>
    <td style="padding:15px">addCallPickupGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateCallPickupGroup(body, callback)</td>
    <td style="padding:15px">updateCallPickupGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetCallPickupGroup(body, callback)</td>
    <td style="padding:15px">getCallPickupGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveCallPickupGroup(body, callback)</td>
    <td style="padding:15px">removeCallPickupGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListCallPickupGroup(body, callback)</td>
    <td style="padding:15px">listCallPickupGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListRoutePlan(body, callback)</td>
    <td style="padding:15px">listRoutePlan</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddGeoLocationPolicy(body, callback)</td>
    <td style="padding:15px">addGeoLocationPolicy</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateGeoLocationPolicy(body, callback)</td>
    <td style="padding:15px">updateGeoLocationPolicy</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetGeoLocationPolicy(body, callback)</td>
    <td style="padding:15px">getGeoLocationPolicy</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveGeoLocationPolicy(body, callback)</td>
    <td style="padding:15px">removeGeoLocationPolicy</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListGeoLocationPolicy(body, callback)</td>
    <td style="padding:15px">listGeoLocationPolicy</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddSipTrunk(body, callback)</td>
    <td style="padding:15px">addSipTrunk</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateSipTrunk(body, callback)</td>
    <td style="padding:15px">updateSipTrunk</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetSipTrunk(body, callback)</td>
    <td style="padding:15px">getSipTrunk</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveSipTrunk(body, callback)</td>
    <td style="padding:15px">removeSipTrunk</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListSipTrunk(body, callback)</td>
    <td style="padding:15px">listSipTrunk</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postResetSipTrunk(body, callback)</td>
    <td style="padding:15px">resetSipTrunk</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRestartSipTrunk(body, callback)</td>
    <td style="padding:15px">restartSipTrunk</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateRegionMatrix(body, callback)</td>
    <td style="padding:15px">updateRegionMatrix</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddCalledPartyTransformationPattern(body, callback)</td>
    <td style="padding:15px">addCalledPartyTransformationPattern</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateCalledPartyTransformationPattern(body, callback)</td>
    <td style="padding:15px">updateCalledPartyTransformationPattern</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetCalledPartyTransformationPattern(body, callback)</td>
    <td style="padding:15px">getCalledPartyTransformationPattern</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveCalledPartyTransformationPattern(body, callback)</td>
    <td style="padding:15px">removeCalledPartyTransformationPattern</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListCalledPartyTransformationPattern(body, callback)</td>
    <td style="padding:15px">listCalledPartyTransformationPattern</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddExternalCallControlProfile(body, callback)</td>
    <td style="padding:15px">addExternalCallControlProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateExternalCallControlProfile(body, callback)</td>
    <td style="padding:15px">updateExternalCallControlProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetExternalCallControlProfile(body, callback)</td>
    <td style="padding:15px">getExternalCallControlProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveExternalCallControlProfile(body, callback)</td>
    <td style="padding:15px">removeExternalCallControlProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListExternalCallControlProfile(body, callback)</td>
    <td style="padding:15px">listExternalCallControlProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddSafSecurityProfile(body, callback)</td>
    <td style="padding:15px">addSafSecurityProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateSafSecurityProfile(body, callback)</td>
    <td style="padding:15px">updateSafSecurityProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetSafSecurityProfile(body, callback)</td>
    <td style="padding:15px">getSafSecurityProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveSafSecurityProfile(body, callback)</td>
    <td style="padding:15px">removeSafSecurityProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListSafSecurityProfile(body, callback)</td>
    <td style="padding:15px">listSafSecurityProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddSafForwarder(body, callback)</td>
    <td style="padding:15px">addSafForwarder</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateSafForwarder(body, callback)</td>
    <td style="padding:15px">updateSafForwarder</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetSafForwarder(body, callback)</td>
    <td style="padding:15px">getSafForwarder</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveSafForwarder(body, callback)</td>
    <td style="padding:15px">removeSafForwarder</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListSafForwarder(body, callback)</td>
    <td style="padding:15px">listSafForwarder</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddCcdHostedDN(body, callback)</td>
    <td style="padding:15px">addCcdHostedDN</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateCcdHostedDN(body, callback)</td>
    <td style="padding:15px">updateCcdHostedDN</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetCcdHostedDN(body, callback)</td>
    <td style="padding:15px">getCcdHostedDN</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveCcdHostedDN(body, callback)</td>
    <td style="padding:15px">removeCcdHostedDN</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListCcdHostedDN(body, callback)</td>
    <td style="padding:15px">listCcdHostedDN</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddCcdHostedDNGroup(body, callback)</td>
    <td style="padding:15px">addCcdHostedDNGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateCcdHostedDNGroup(body, callback)</td>
    <td style="padding:15px">updateCcdHostedDNGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetCcdHostedDNGroup(body, callback)</td>
    <td style="padding:15px">getCcdHostedDNGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveCcdHostedDNGroup(body, callback)</td>
    <td style="padding:15px">removeCcdHostedDNGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListCcdHostedDNGroup(body, callback)</td>
    <td style="padding:15px">listCcdHostedDNGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddCcdRequestingService(body, callback)</td>
    <td style="padding:15px">addCcdRequestingService</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateCcdRequestingService(body, callback)</td>
    <td style="padding:15px">updateCcdRequestingService</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetCcdRequestingService(body, callback)</td>
    <td style="padding:15px">getCcdRequestingService</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveCcdRequestingService(body, callback)</td>
    <td style="padding:15px">removeCcdRequestingService</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateInterClusterServiceProfile(body, callback)</td>
    <td style="padding:15px">updateInterClusterServiceProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetInterClusterServiceProfile(body, callback)</td>
    <td style="padding:15px">getInterClusterServiceProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddRemoteCluster(body, callback)</td>
    <td style="padding:15px">addRemoteCluster</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateRemoteCluster(body, callback)</td>
    <td style="padding:15px">updateRemoteCluster</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetRemoteCluster(body, callback)</td>
    <td style="padding:15px">getRemoteCluster</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveRemoteCluster(body, callback)</td>
    <td style="padding:15px">removeRemoteCluster</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListRemoteCluster(body, callback)</td>
    <td style="padding:15px">listRemoteCluster</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddCcdAdvertisingService(body, callback)</td>
    <td style="padding:15px">addCcdAdvertisingService</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateCcdAdvertisingService(body, callback)</td>
    <td style="padding:15px">updateCcdAdvertisingService</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetCcdAdvertisingService(body, callback)</td>
    <td style="padding:15px">getCcdAdvertisingService</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveCcdAdvertisingService(body, callback)</td>
    <td style="padding:15px">removeCcdAdvertisingService</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListCcdAdvertisingService(body, callback)</td>
    <td style="padding:15px">listCcdAdvertisingService</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddUnitsToGateway(body, callback)</td>
    <td style="padding:15px">addUnitsToGateway</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddGatewaySubunits(body, callback)</td>
    <td style="padding:15px">addGatewaySubunits</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddLdapDirectory(body, callback)</td>
    <td style="padding:15px">addLdapDirectory</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateLdapDirectory(body, callback)</td>
    <td style="padding:15px">updateLdapDirectory</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetLdapDirectory(body, callback)</td>
    <td style="padding:15px">getLdapDirectory</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveLdapDirectory(body, callback)</td>
    <td style="padding:15px">removeLdapDirectory</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListLdapDirectory(body, callback)</td>
    <td style="padding:15px">listLdapDirectory</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateEmccFeatureConfig(body, callback)</td>
    <td style="padding:15px">updateEmccFeatureConfig</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetEmccFeatureConfig(body, callback)</td>
    <td style="padding:15px">getEmccFeatureConfig</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddSafCcdPurgeBlockLearnedRoutes(body, callback)</td>
    <td style="padding:15px">addSafCcdPurgeBlockLearnedRoutes</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateSafCcdPurgeBlockLearnedRoutes(body, callback)</td>
    <td style="padding:15px">updateSafCcdPurgeBlockLearnedRoutes</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetSafCcdPurgeBlockLearnedRoutes(body, callback)</td>
    <td style="padding:15px">getSafCcdPurgeBlockLearnedRoutes</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveSafCcdPurgeBlockLearnedRoutes(body, callback)</td>
    <td style="padding:15px">removeSafCcdPurgeBlockLearnedRoutes</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListSafCcdPurgeBlockLearnedRoutes(body, callback)</td>
    <td style="padding:15px">listSafCcdPurgeBlockLearnedRoutes</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddVpnGateway(body, callback)</td>
    <td style="padding:15px">addVpnGateway</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateVpnGateway(body, callback)</td>
    <td style="padding:15px">updateVpnGateway</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetVpnGateway(body, callback)</td>
    <td style="padding:15px">getVpnGateway</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveVpnGateway(body, callback)</td>
    <td style="padding:15px">removeVpnGateway</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListVpnGateway(body, callback)</td>
    <td style="padding:15px">listVpnGateway</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddVpnGroup(body, callback)</td>
    <td style="padding:15px">addVpnGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateVpnGroup(body, callback)</td>
    <td style="padding:15px">updateVpnGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetVpnGroup(body, callback)</td>
    <td style="padding:15px">getVpnGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveVpnGroup(body, callback)</td>
    <td style="padding:15px">removeVpnGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListVpnGroup(body, callback)</td>
    <td style="padding:15px">listVpnGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddVpnProfile(body, callback)</td>
    <td style="padding:15px">addVpnProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateVpnProfile(body, callback)</td>
    <td style="padding:15px">updateVpnProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetVpnProfile(body, callback)</td>
    <td style="padding:15px">getVpnProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveVpnProfile(body, callback)</td>
    <td style="padding:15px">removeVpnProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListVpnProfile(body, callback)</td>
    <td style="padding:15px">listVpnProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddImeServer(body, callback)</td>
    <td style="padding:15px">addImeServer</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateImeServer(body, callback)</td>
    <td style="padding:15px">updateImeServer</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetImeServer(body, callback)</td>
    <td style="padding:15px">getImeServer</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveImeServer(body, callback)</td>
    <td style="padding:15px">removeImeServer</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListImeServer(body, callback)</td>
    <td style="padding:15px">listImeServer</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddImeRouteFilterGroup(body, callback)</td>
    <td style="padding:15px">addImeRouteFilterGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateImeRouteFilterGroup(body, callback)</td>
    <td style="padding:15px">updateImeRouteFilterGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetImeRouteFilterGroup(body, callback)</td>
    <td style="padding:15px">getImeRouteFilterGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveImeRouteFilterGroup(body, callback)</td>
    <td style="padding:15px">removeImeRouteFilterGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListImeRouteFilterGroup(body, callback)</td>
    <td style="padding:15px">listImeRouteFilterGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddImeRouteFilterElement(body, callback)</td>
    <td style="padding:15px">addImeRouteFilterElement</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateImeRouteFilterElement(body, callback)</td>
    <td style="padding:15px">updateImeRouteFilterElement</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetImeRouteFilterElement(body, callback)</td>
    <td style="padding:15px">getImeRouteFilterElement</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveImeRouteFilterElement(body, callback)</td>
    <td style="padding:15px">removeImeRouteFilterElement</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListImeRouteFilterElement(body, callback)</td>
    <td style="padding:15px">listImeRouteFilterElement</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddImeClient(body, callback)</td>
    <td style="padding:15px">addImeClient</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateImeClient(body, callback)</td>
    <td style="padding:15px">updateImeClient</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetImeClient(body, callback)</td>
    <td style="padding:15px">getImeClient</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveImeClient(body, callback)</td>
    <td style="padding:15px">removeImeClient</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListImeClient(body, callback)</td>
    <td style="padding:15px">listImeClient</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddImeEnrolledPattern(body, callback)</td>
    <td style="padding:15px">addImeEnrolledPattern</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateImeEnrolledPattern(body, callback)</td>
    <td style="padding:15px">updateImeEnrolledPattern</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetImeEnrolledPattern(body, callback)</td>
    <td style="padding:15px">getImeEnrolledPattern</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveImeEnrolledPattern(body, callback)</td>
    <td style="padding:15px">removeImeEnrolledPattern</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListImeEnrolledPattern(body, callback)</td>
    <td style="padding:15px">listImeEnrolledPattern</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddImeEnrolledPatternGroup(body, callback)</td>
    <td style="padding:15px">addImeEnrolledPatternGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateImeEnrolledPatternGroup(body, callback)</td>
    <td style="padding:15px">updateImeEnrolledPatternGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetImeEnrolledPatternGroup(body, callback)</td>
    <td style="padding:15px">getImeEnrolledPatternGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveImeEnrolledPatternGroup(body, callback)</td>
    <td style="padding:15px">removeImeEnrolledPatternGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListImeEnrolledPatternGroup(body, callback)</td>
    <td style="padding:15px">listImeEnrolledPatternGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddImeExclusionNumber(body, callback)</td>
    <td style="padding:15px">addImeExclusionNumber</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateImeExclusionNumber(body, callback)</td>
    <td style="padding:15px">updateImeExclusionNumber</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetImeExclusionNumber(body, callback)</td>
    <td style="padding:15px">getImeExclusionNumber</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveImeExclusionNumber(body, callback)</td>
    <td style="padding:15px">removeImeExclusionNumber</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListImeExclusionNumber(body, callback)</td>
    <td style="padding:15px">listImeExclusionNumber</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddImeExclusionNumberGroup(body, callback)</td>
    <td style="padding:15px">addImeExclusionNumberGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateImeExclusionNumberGroup(body, callback)</td>
    <td style="padding:15px">updateImeExclusionNumberGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetImeExclusionNumberGroup(body, callback)</td>
    <td style="padding:15px">getImeExclusionNumberGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveImeExclusionNumberGroup(body, callback)</td>
    <td style="padding:15px">removeImeExclusionNumberGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListImeExclusionNumberGroup(body, callback)</td>
    <td style="padding:15px">listImeExclusionNumberGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddImeFirewall(body, callback)</td>
    <td style="padding:15px">addImeFirewall</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateImeFirewall(body, callback)</td>
    <td style="padding:15px">updateImeFirewall</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetImeFirewall(body, callback)</td>
    <td style="padding:15px">getImeFirewall</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveImeFirewall(body, callback)</td>
    <td style="padding:15px">removeImeFirewall</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListImeFirewall(body, callback)</td>
    <td style="padding:15px">listImeFirewall</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddImeE164Transformation(body, callback)</td>
    <td style="padding:15px">addImeE164Transformation</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateImeE164Transformation(body, callback)</td>
    <td style="padding:15px">updateImeE164Transformation</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetImeE164Transformation(body, callback)</td>
    <td style="padding:15px">getImeE164Transformation</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveImeE164Transformation(body, callback)</td>
    <td style="padding:15px">removeImeE164Transformation</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListImeE164Transformation(body, callback)</td>
    <td style="padding:15px">listImeE164Transformation</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddTransformationProfile(body, callback)</td>
    <td style="padding:15px">addTransformationProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateTransformationProfile(body, callback)</td>
    <td style="padding:15px">updateTransformationProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetTransformationProfile(body, callback)</td>
    <td style="padding:15px">getTransformationProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveTransformationProfile(body, callback)</td>
    <td style="padding:15px">removeTransformationProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListTransformationProfile(body, callback)</td>
    <td style="padding:15px">listTransformationProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddFallbackProfile(body, callback)</td>
    <td style="padding:15px">addFallbackProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateFallbackProfile(body, callback)</td>
    <td style="padding:15px">updateFallbackProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetFallbackProfile(body, callback)</td>
    <td style="padding:15px">getFallbackProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveFallbackProfile(body, callback)</td>
    <td style="padding:15px">removeFallbackProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListFallbackProfile(body, callback)</td>
    <td style="padding:15px">listFallbackProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddLdapFilter(body, callback)</td>
    <td style="padding:15px">addLdapFilter</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateLdapFilter(body, callback)</td>
    <td style="padding:15px">updateLdapFilter</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetLdapFilter(body, callback)</td>
    <td style="padding:15px">getLdapFilter</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveLdapFilter(body, callback)</td>
    <td style="padding:15px">removeLdapFilter</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListLdapFilter(body, callback)</td>
    <td style="padding:15px">listLdapFilter</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddAppServerInfo(body, callback)</td>
    <td style="padding:15px">addAppServerInfo</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateAppServerInfo(body, callback)</td>
    <td style="padding:15px">updateAppServerInfo</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateTvsCertificate(body, callback)</td>
    <td style="padding:15px">updateTvsCertificate</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetTvsCertificate(body, callback)</td>
    <td style="padding:15px">getTvsCertificate</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListTvsCertificate(body, callback)</td>
    <td style="padding:15px">listTvsCertificate</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddFeatureControlPolicy(body, callback)</td>
    <td style="padding:15px">addFeatureControlPolicy</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateFeatureControlPolicy(body, callback)</td>
    <td style="padding:15px">updateFeatureControlPolicy</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetFeatureControlPolicy(body, callback)</td>
    <td style="padding:15px">getFeatureControlPolicy</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveFeatureControlPolicy(body, callback)</td>
    <td style="padding:15px">removeFeatureControlPolicy</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListFeatureControlPolicy(body, callback)</td>
    <td style="padding:15px">listFeatureControlPolicy</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddMobilityProfile(body, callback)</td>
    <td style="padding:15px">addMobilityProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateMobilityProfile(body, callback)</td>
    <td style="padding:15px">updateMobilityProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetMobilityProfile(body, callback)</td>
    <td style="padding:15px">getMobilityProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveMobilityProfile(body, callback)</td>
    <td style="padding:15px">removeMobilityProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListMobilityProfile(body, callback)</td>
    <td style="padding:15px">listMobilityProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddEnterpriseFeatureAccessConfiguration(body, callback)</td>
    <td style="padding:15px">addEnterpriseFeatureAccessConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateEnterpriseFeatureAccessConfiguration(body, callback)</td>
    <td style="padding:15px">updateEnterpriseFeatureAccessConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetEnterpriseFeatureAccessConfiguration(body, callback)</td>
    <td style="padding:15px">getEnterpriseFeatureAccessConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveEnterpriseFeatureAccessConfiguration(body, callback)</td>
    <td style="padding:15px">removeEnterpriseFeatureAccessConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListEnterpriseFeatureAccessConfiguration(body, callback)</td>
    <td style="padding:15px">listEnterpriseFeatureAccessConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddHandoffConfiguration(body, callback)</td>
    <td style="padding:15px">addHandoffConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateHandoffConfiguration(body, callback)</td>
    <td style="padding:15px">updateHandoffConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetHandoffConfiguration(body, callback)</td>
    <td style="padding:15px">getHandoffConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveHandoffConfiguration(body, callback)</td>
    <td style="padding:15px">removeHandoffConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddCalledPartyTracing(body, callback)</td>
    <td style="padding:15px">addCalledPartyTracing</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveCalledPartyTracing(body, callback)</td>
    <td style="padding:15px">removeCalledPartyTracing</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListCalledPartyTracing(body, callback)</td>
    <td style="padding:15px">listCalledPartyTracing</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddSIPNormalizationScript(body, callback)</td>
    <td style="padding:15px">addSIPNormalizationScript</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateSIPNormalizationScript(body, callback)</td>
    <td style="padding:15px">updateSIPNormalizationScript</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetSIPNormalizationScript(body, callback)</td>
    <td style="padding:15px">getSIPNormalizationScript</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveSIPNormalizationScript(body, callback)</td>
    <td style="padding:15px">removeSIPNormalizationScript</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListSIPNormalizationScript(body, callback)</td>
    <td style="padding:15px">listSIPNormalizationScript</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddCustomUserField(body, callback)</td>
    <td style="padding:15px">addCustomUserField</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateCustomUserField(body, callback)</td>
    <td style="padding:15px">updateCustomUserField</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetCustomUserField(body, callback)</td>
    <td style="padding:15px">getCustomUserField</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveCustomUserField(body, callback)</td>
    <td style="padding:15px">removeCustomUserField</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListCustomUserField(body, callback)</td>
    <td style="padding:15px">listCustomUserField</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddGatewaySccpEndpoints(body, callback)</td>
    <td style="padding:15px">addGatewaySccpEndpoints</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateGatewaySccpEndpoints(body, callback)</td>
    <td style="padding:15px">updateGatewaySccpEndpoints</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetGatewaySccpEndpoints(body, callback)</td>
    <td style="padding:15px">getGatewaySccpEndpoints</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveGatewaySccpEndpoints(body, callback)</td>
    <td style="padding:15px">removeGatewaySccpEndpoints</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddBillingServer(body, callback)</td>
    <td style="padding:15px">addBillingServer</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListBillingServer(body, callback)</td>
    <td style="padding:15px">listBillingServer</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddLbmGroup(body, callback)</td>
    <td style="padding:15px">addLbmGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateLbmGroup(body, callback)</td>
    <td style="padding:15px">updateLbmGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetLbmGroup(body, callback)</td>
    <td style="padding:15px">getLbmGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveLbmGroup(body, callback)</td>
    <td style="padding:15px">removeLbmGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListLbmGroup(body, callback)</td>
    <td style="padding:15px">listLbmGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddAnnouncement(body, callback)</td>
    <td style="padding:15px">addAnnouncement</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateAnnouncement(body, callback)</td>
    <td style="padding:15px">updateAnnouncement</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetAnnouncement(body, callback)</td>
    <td style="padding:15px">getAnnouncement</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveAnnouncement(body, callback)</td>
    <td style="padding:15px">removeAnnouncement</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListAnnouncement(body, callback)</td>
    <td style="padding:15px">listAnnouncement</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddServiceProfile(body, callback)</td>
    <td style="padding:15px">addServiceProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateServiceProfile(body, callback)</td>
    <td style="padding:15px">updateServiceProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetServiceProfile(body, callback)</td>
    <td style="padding:15px">getServiceProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveServiceProfile(body, callback)</td>
    <td style="padding:15px">removeServiceProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListServiceProfile(body, callback)</td>
    <td style="padding:15px">listServiceProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddLdapSyncCustomField(body, callback)</td>
    <td style="padding:15px">addLdapSyncCustomField</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateLdapSyncCustomField(body, callback)</td>
    <td style="padding:15px">updateLdapSyncCustomField</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetLdapSyncCustomField(body, callback)</td>
    <td style="padding:15px">getLdapSyncCustomField</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveLdapSyncCustomField(body, callback)</td>
    <td style="padding:15px">removeLdapSyncCustomField</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddAudioCodecPreferenceList(body, callback)</td>
    <td style="padding:15px">addAudioCodecPreferenceList</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateAudioCodecPreferenceList(body, callback)</td>
    <td style="padding:15px">updateAudioCodecPreferenceList</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetAudioCodecPreferenceList(body, callback)</td>
    <td style="padding:15px">getAudioCodecPreferenceList</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveAudioCodecPreferenceList(body, callback)</td>
    <td style="padding:15px">removeAudioCodecPreferenceList</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListAudioCodecPreferenceList(body, callback)</td>
    <td style="padding:15px">listAudioCodecPreferenceList</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddUcService(body, callback)</td>
    <td style="padding:15px">addUcService</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateUcService(body, callback)</td>
    <td style="padding:15px">updateUcService</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetUcService(body, callback)</td>
    <td style="padding:15px">getUcService</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveUcService(body, callback)</td>
    <td style="padding:15px">removeUcService</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListUcService(body, callback)</td>
    <td style="padding:15px">listUcService</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddLbmHubGroup(body, callback)</td>
    <td style="padding:15px">addLbmHubGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateLbmHubGroup(body, callback)</td>
    <td style="padding:15px">updateLbmHubGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetLbmHubGroup(body, callback)</td>
    <td style="padding:15px">getLbmHubGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveLbmHubGroup(body, callback)</td>
    <td style="padding:15px">removeLbmHubGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListLbmHubGroup(body, callback)</td>
    <td style="padding:15px">listLbmHubGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddImportedDirectoryUriCatalogs(body, callback)</td>
    <td style="padding:15px">addImportedDirectoryUriCatalogs</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateImportedDirectoryUriCatalogs(body, callback)</td>
    <td style="padding:15px">updateImportedDirectoryUriCatalogs</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetImportedDirectoryUriCatalogs(body, callback)</td>
    <td style="padding:15px">getImportedDirectoryUriCatalogs</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveImportedDirectoryUriCatalogs(body, callback)</td>
    <td style="padding:15px">removeImportedDirectoryUriCatalogs</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListImportedDirectoryUriCatalogs(body, callback)</td>
    <td style="padding:15px">listImportedDirectoryUriCatalogs</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddVohServer(body, callback)</td>
    <td style="padding:15px">addVohServer</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateVohServer(body, callback)</td>
    <td style="padding:15px">updateVohServer</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetVohServer(body, callback)</td>
    <td style="padding:15px">getVohServer</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveVohServer(body, callback)</td>
    <td style="padding:15px">removeVohServer</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListVohServer(body, callback)</td>
    <td style="padding:15px">listVohServer</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddSdpTransparencyProfile(body, callback)</td>
    <td style="padding:15px">addSdpTransparencyProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateSdpTransparencyProfile(body, callback)</td>
    <td style="padding:15px">updateSdpTransparencyProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetSdpTransparencyProfile(body, callback)</td>
    <td style="padding:15px">getSdpTransparencyProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveSdpTransparencyProfile(body, callback)</td>
    <td style="padding:15px">removeSdpTransparencyProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListSdpTransparencyProfile(body, callback)</td>
    <td style="padding:15px">listSdpTransparencyProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddFeatureGroupTemplate(body, callback)</td>
    <td style="padding:15px">addFeatureGroupTemplate</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateFeatureGroupTemplate(body, callback)</td>
    <td style="padding:15px">updateFeatureGroupTemplate</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetFeatureGroupTemplate(body, callback)</td>
    <td style="padding:15px">getFeatureGroupTemplate</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveFeatureGroupTemplate(body, callback)</td>
    <td style="padding:15px">removeFeatureGroupTemplate</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListFeatureGroupTemplate(body, callback)</td>
    <td style="padding:15px">listFeatureGroupTemplate</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddDirNumberAliasLookupandSync(body, callback)</td>
    <td style="padding:15px">addDirNumberAliasLookupandSync</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateDirNumberAliasLookupandSync(body, callback)</td>
    <td style="padding:15px">updateDirNumberAliasLookupandSync</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetDirNumberAliasLookupandSync(body, callback)</td>
    <td style="padding:15px">getDirNumberAliasLookupandSync</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveDirNumberAliasLookupandSync(body, callback)</td>
    <td style="padding:15px">removeDirNumberAliasLookupandSync</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListDirNumberAliasLookupandSync(body, callback)</td>
    <td style="padding:15px">listDirNumberAliasLookupandSync</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddLocalRouteGroup(body, callback)</td>
    <td style="padding:15px">addLocalRouteGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetLocalRouteGroup(body, callback)</td>
    <td style="padding:15px">getLocalRouteGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveLocalRouteGroup(body, callback)</td>
    <td style="padding:15px">removeLocalRouteGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListLocalRouteGroup(body, callback)</td>
    <td style="padding:15px">listLocalRouteGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddAdvertisedPatterns(body, callback)</td>
    <td style="padding:15px">addAdvertisedPatterns</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateAdvertisedPatterns(body, callback)</td>
    <td style="padding:15px">updateAdvertisedPatterns</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetAdvertisedPatterns(body, callback)</td>
    <td style="padding:15px">getAdvertisedPatterns</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveAdvertisedPatterns(body, callback)</td>
    <td style="padding:15px">removeAdvertisedPatterns</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListAdvertisedPatterns(body, callback)</td>
    <td style="padding:15px">listAdvertisedPatterns</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddBlockedLearnedPatterns(body, callback)</td>
    <td style="padding:15px">addBlockedLearnedPatterns</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateBlockedLearnedPatterns(body, callback)</td>
    <td style="padding:15px">updateBlockedLearnedPatterns</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetBlockedLearnedPatterns(body, callback)</td>
    <td style="padding:15px">getBlockedLearnedPatterns</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveBlockedLearnedPatterns(body, callback)</td>
    <td style="padding:15px">removeBlockedLearnedPatterns</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListBlockedLearnedPatterns(body, callback)</td>
    <td style="padding:15px">listBlockedLearnedPatterns</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddCCAProfiles(body, callback)</td>
    <td style="padding:15px">addCCAProfiles</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateCCAProfiles(body, callback)</td>
    <td style="padding:15px">updateCCAProfiles</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetCCAProfiles(body, callback)</td>
    <td style="padding:15px">getCCAProfiles</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveCCAProfiles(body, callback)</td>
    <td style="padding:15px">removeCCAProfiles</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListCCAProfiles(body, callback)</td>
    <td style="padding:15px">listCCAProfiles</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddUniversalDeviceTemplate(body, callback)</td>
    <td style="padding:15px">addUniversalDeviceTemplate</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateUniversalDeviceTemplate(body, callback)</td>
    <td style="padding:15px">updateUniversalDeviceTemplate</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetUniversalDeviceTemplate(body, callback)</td>
    <td style="padding:15px">getUniversalDeviceTemplate</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveUniversalDeviceTemplate(body, callback)</td>
    <td style="padding:15px">removeUniversalDeviceTemplate</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListUniversalDeviceTemplate(body, callback)</td>
    <td style="padding:15px">listUniversalDeviceTemplate</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddUserProfileProvision(body, callback)</td>
    <td style="padding:15px">addUserProfileProvision</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateUserProfileProvision(body, callback)</td>
    <td style="padding:15px">updateUserProfileProvision</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetUserProfileProvision(body, callback)</td>
    <td style="padding:15px">getUserProfileProvision</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveUserProfileProvision(body, callback)</td>
    <td style="padding:15px">removeUserProfileProvision</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListUserProfileProvision(body, callback)</td>
    <td style="padding:15px">listUserProfileProvision</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddPresenceRedundancyGroup(body, callback)</td>
    <td style="padding:15px">addPresenceRedundancyGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdatePresenceRedundancyGroup(body, callback)</td>
    <td style="padding:15px">updatePresenceRedundancyGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetPresenceRedundancyGroup(body, callback)</td>
    <td style="padding:15px">getPresenceRedundancyGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemovePresenceRedundancyGroup(body, callback)</td>
    <td style="padding:15px">removePresenceRedundancyGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListPresenceRedundancyGroup(body, callback)</td>
    <td style="padding:15px">listPresenceRedundancyGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListAssignedPresenceServers(body, callback)</td>
    <td style="padding:15px">listAssignedPresenceServers</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListUnassignedPresenceServers(body, callback)</td>
    <td style="padding:15px">listUnassignedPresenceServers</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListAssignedPresenceUsers(body, callback)</td>
    <td style="padding:15px">listAssignedPresenceUsers</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListUnassignedPresenceUsers(body, callback)</td>
    <td style="padding:15px">listUnassignedPresenceUsers</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddWifiHotspot(body, callback)</td>
    <td style="padding:15px">addWifiHotspot</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateWifiHotspot(body, callback)</td>
    <td style="padding:15px">updateWifiHotspot</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetWifiHotspot(body, callback)</td>
    <td style="padding:15px">getWifiHotspot</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveWifiHotspot(body, callback)</td>
    <td style="padding:15px">removeWifiHotspot</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListWifiHotspot(body, callback)</td>
    <td style="padding:15px">listWifiHotspot</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddWlanProfileGroup(body, callback)</td>
    <td style="padding:15px">addWlanProfileGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateWlanProfileGroup(body, callback)</td>
    <td style="padding:15px">updateWlanProfileGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetWlanProfileGroup(body, callback)</td>
    <td style="padding:15px">getWlanProfileGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveWlanProfileGroup(body, callback)</td>
    <td style="padding:15px">removeWlanProfileGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListWlanProfileGroup(body, callback)</td>
    <td style="padding:15px">listWlanProfileGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddWLANProfile(body, callback)</td>
    <td style="padding:15px">addWLANProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateWLANProfile(body, callback)</td>
    <td style="padding:15px">updateWLANProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetWLANProfile(body, callback)</td>
    <td style="padding:15px">getWLANProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveWLANProfile(body, callback)</td>
    <td style="padding:15px">removeWLANProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListWLANProfile(body, callback)</td>
    <td style="padding:15px">listWLANProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddUniversalLineTemplate(body, callback)</td>
    <td style="padding:15px">addUniversalLineTemplate</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateUniversalLineTemplate(body, callback)</td>
    <td style="padding:15px">updateUniversalLineTemplate</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetUniversalLineTemplate(body, callback)</td>
    <td style="padding:15px">getUniversalLineTemplate</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveUniversalLineTemplate(body, callback)</td>
    <td style="padding:15px">removeUniversalLineTemplate</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListUniversalLineTemplate(body, callback)</td>
    <td style="padding:15px">listUniversalLineTemplate</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddNetworkAccessProfile(body, callback)</td>
    <td style="padding:15px">addNetworkAccessProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateNetworkAccessProfile(body, callback)</td>
    <td style="padding:15px">updateNetworkAccessProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetNetworkAccessProfile(body, callback)</td>
    <td style="padding:15px">getNetworkAccessProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveNetworkAccessProfile(body, callback)</td>
    <td style="padding:15px">removeNetworkAccessProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListNetworkAccessProfile(body, callback)</td>
    <td style="padding:15px">listNetworkAccessProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetLicensedUser(body, callback)</td>
    <td style="padding:15px">getLicensedUser</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListLicensedUser(body, callback)</td>
    <td style="padding:15px">listLicensedUser</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddHttpProfile(body, callback)</td>
    <td style="padding:15px">addHttpProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateHttpProfile(body, callback)</td>
    <td style="padding:15px">updateHttpProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetHttpProfile(body, callback)</td>
    <td style="padding:15px">getHttpProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveHttpProfile(body, callback)</td>
    <td style="padding:15px">removeHttpProfile</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddElinGroup(body, callback)</td>
    <td style="padding:15px">addElinGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateElinGroup(body, callback)</td>
    <td style="padding:15px">updateElinGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetElinGroup(body, callback)</td>
    <td style="padding:15px">getElinGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveElinGroup(body, callback)</td>
    <td style="padding:15px">removeElinGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListElinGroup(body, callback)</td>
    <td style="padding:15px">listElinGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateSecureConfig(body, callback)</td>
    <td style="padding:15px">updateSecureConfig</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetSecureConfig(body, callback)</td>
    <td style="padding:15px">getSecureConfig</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListUnassignedDevice(body, callback)</td>
    <td style="padding:15px">listUnassignedDevice</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetRegistrationDynamic(body, callback)</td>
    <td style="padding:15px">getRegistrationDynamic</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListRegistrationDynamic(body, callback)</td>
    <td style="padding:15px">listRegistrationDynamic</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddInfrastructureDevice(body, callback)</td>
    <td style="padding:15px">addInfrastructureDevice</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateInfrastructureDevice(body, callback)</td>
    <td style="padding:15px">updateInfrastructureDevice</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetInfrastructureDevice(body, callback)</td>
    <td style="padding:15px">getInfrastructureDevice</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveInfrastructureDevice(body, callback)</td>
    <td style="padding:15px">removeInfrastructureDevice</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListInfrastructureDevice(body, callback)</td>
    <td style="padding:15px">listInfrastructureDevice</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateLdapSearch(body, callback)</td>
    <td style="padding:15px">updateLdapSearch</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetLdapSearch(body, callback)</td>
    <td style="padding:15px">getLdapSearch</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListLdapSearch(body, callback)</td>
    <td style="padding:15px">listLdapSearch</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddWirelessAccessPointControllers(body, callback)</td>
    <td style="padding:15px">addWirelessAccessPointControllers</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateWirelessAccessPointControllers(body, callback)</td>
    <td style="padding:15px">updateWirelessAccessPointControllers</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetWirelessAccessPointControllers(body, callback)</td>
    <td style="padding:15px">getWirelessAccessPointControllers</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveWirelessAccessPointControllers(body, callback)</td>
    <td style="padding:15px">removeWirelessAccessPointControllers</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListWirelessAccessPointControllers(body, callback)</td>
    <td style="padding:15px">listWirelessAccessPointControllers</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postExecuteSQLQuery(body, callback)</td>
    <td style="padding:15px">executeSQLQuery</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postExecuteSQLUpdate(body, callback)</td>
    <td style="padding:15px">executeSQLUpdate</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDoAuthenticateUser(body, callback)</td>
    <td style="padding:15px">doAuthenticateUser</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDoDeviceLogin(body, callback)</td>
    <td style="padding:15px">doDeviceLogin</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDoDeviceLogout(body, callback)</td>
    <td style="padding:15px">doDeviceLogout</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDoDeviceReset(body, callback)</td>
    <td style="padding:15px">doDeviceReset</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetOSVersion(body, callback)</td>
    <td style="padding:15px">getOSVersion</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetNumDevices(body, callback)</td>
    <td style="padding:15px">getNumDevices</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddMobility(body, callback)</td>
    <td style="padding:15px">addMobility</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateMobility(body, callback)</td>
    <td style="padding:15px">updateMobility</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetMobility(body, callback)</td>
    <td style="padding:15px">getMobility</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetEnterprisePhoneConfig(body, callback)</td>
    <td style="padding:15px">getEnterprisePhoneConfig</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateEnterprisePhoneConfig(body, callback)</td>
    <td style="padding:15px">updateEnterprisePhoneConfig</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetLdapSystem(body, callback)</td>
    <td style="padding:15px">getLdapSystem</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateLdapSystem(body, callback)</td>
    <td style="padding:15px">updateLdapSystem</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetLdapAuthentication(body, callback)</td>
    <td style="padding:15px">getLdapAuthentication</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateLdapAuthentication(body, callback)</td>
    <td style="padding:15px">updateLdapAuthentication</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddApplicationToSoftkeyTemplate(body, callback)</td>
    <td style="padding:15px">addApplicationToSoftkeyTemplate</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveApplicationToSoftkeyTemplate(body, callback)</td>
    <td style="padding:15px">removeApplicationToSoftkeyTemplate</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetCCMVersion(body, callback)</td>
    <td style="padding:15px">getCCMVersion</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveUnitsToGateway(body, callback)</td>
    <td style="padding:15px">removeUnitsToGateway</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveGatewaySubunits(body, callback)</td>
    <td style="padding:15px">removeGatewaySubunits</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateImeFeatureConfig(body, callback)</td>
    <td style="padding:15px">updateImeFeatureConfig</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateFallbackFeatureConfig(body, callback)</td>
    <td style="padding:15px">updateFallbackFeatureConfig</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveImeLearnedRoutes(body, callback)</td>
    <td style="padding:15px">removeImeLearnedRoutes</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateImeLearnedRoutes(body, callback)</td>
    <td style="padding:15px">updateImeLearnedRoutes</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetImeLearnedRoutes(body, callback)</td>
    <td style="padding:15px">getImeLearnedRoutes</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetImeFeatureConfig(body, callback)</td>
    <td style="padding:15px">getImeFeatureConfig</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetFallbackFeatureConfig(body, callback)</td>
    <td style="padding:15px">getFallbackFeatureConfig</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetAppServerInfo(body, callback)</td>
    <td style="padding:15px">getAppServerInfo</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveAppServerInfo(body, callback)</td>
    <td style="padding:15px">removeAppServerInfo</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDoLdapSync(body, callback)</td>
    <td style="padding:15px">doLdapSync</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetLdapSyncStatus(body, callback)</td>
    <td style="padding:15px">getLdapSyncStatus</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateSoftKeySet(body, callback)</td>
    <td style="padding:15px">updateSoftKeySet</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetSoftKeySet(body, callback)</td>
    <td style="padding:15px">getSoftKeySet</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDoUpdateRemoteCluster(body, callback)</td>
    <td style="padding:15px">doUpdateRemoteCluster</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateSyslogConfiguration(body, callback)</td>
    <td style="padding:15px">updateSyslogConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetSyslogConfiguration(body, callback)</td>
    <td style="padding:15px">getSyslogConfiguration</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListLdapSyncCustomField(body, callback)</td>
    <td style="padding:15px">listLdapSyncCustomField</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetPhoneTypeDisplayInstance(body, callback)</td>
    <td style="padding:15px">getPhoneTypeDisplayInstance</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateInterClusterDirectoryUri(body, callback)</td>
    <td style="padding:15px">updateInterClusterDirectoryUri</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateIlsConfig(body, callback)</td>
    <td style="padding:15px">updateIlsConfig</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetIlsConfig(body, callback)</td>
    <td style="padding:15px">getIlsConfig</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddSNMPCommunityString(body, callback)</td>
    <td style="padding:15px">addSNMPCommunityString</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveSNMPCommunityString(body, callback)</td>
    <td style="padding:15px">removeSNMPCommunityString</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetSNMPCommunityString(body, callback)</td>
    <td style="padding:15px">getSNMPCommunityString</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateSNMPCommunityString(body, callback)</td>
    <td style="padding:15px">updateSNMPCommunityString</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAddSNMPUser(body, callback)</td>
    <td style="padding:15px">addSNMPUser</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetSNMPUser(body, callback)</td>
    <td style="padding:15px">getSNMPUser</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveSNMPUser(body, callback)</td>
    <td style="padding:15px">removeSNMPUser</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateSNMPUser(body, callback)</td>
    <td style="padding:15px">updateSNMPUser</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetSNMPMIB2List(body, callback)</td>
    <td style="padding:15px">getSNMPMIB2List</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateSNMPMIB2List(body, callback)</td>
    <td style="padding:15px">updateSNMPMIB2List</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateBillingServer(body, callback)</td>
    <td style="padding:15px">updateBillingServer</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetBillingServer(body, callback)</td>
    <td style="padding:15px">getBillingServer</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemoveBillingServer(body, callback)</td>
    <td style="padding:15px">removeBillingServer</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateCcdFeatureConfig(body, callback)</td>
    <td style="padding:15px">updateCcdFeatureConfig</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetCcdFeatureConfig(body, callback)</td>
    <td style="padding:15px">getCcdFeatureConfig</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateRoutePartitionsForLearnedPatterns(body, callback)</td>
    <td style="padding:15px">updateRoutePartitionsForLearnedPatterns</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateLocalRouteGroup(body, callback)</td>
    <td style="padding:15px">updateLocalRouteGroup</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdatePageLayoutPreferences(body, callback)</td>
    <td style="padding:15px">updatePageLayoutPreferences</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetPageLayoutPreferences(body, callback)</td>
    <td style="padding:15px">getPageLayoutPreferences</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAssignPresenceUser(body, callback)</td>
    <td style="padding:15px">assignPresenceUser</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUnassignPresenceUser(body, callback)</td>
    <td style="padding:15px">unassignPresenceUser</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetCredentialPolicyDefault(body, callback)</td>
    <td style="padding:15px">getCredentialPolicyDefault</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateCredentialPolicyDefault(body, callback)</td>
    <td style="padding:15px">updateCredentialPolicyDefault</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateSelfProvisioning(body, callback)</td>
    <td style="padding:15px">updateSelfProvisioning</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postListChange(body, callback)</td>
    <td style="padding:15px">listChange</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDoChangeDNDStatus(body, callback)</td>
    <td style="padding:15px">doChangeDNDStatus</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDoUpdateLicenseUsage(body, callback)</td>
    <td style="padding:15px">doUpdateLicenseUsage</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDoServiceParametersReset(body, callback)</td>
    <td style="padding:15px">doServiceParametersReset</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDoEnterpriseParametersReset(body, callback)</td>
    <td style="padding:15px">doEnterpriseParametersReset</td>
    <td style="padding:15px">{base_path}/{version}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
