
## 0.4.4 [10-15-2024]

* Changes made at 2024.10.14_20:51PM

See merge request itentialopensource/adapters/adapter-cisco_callmanager!17

---

## 0.4.3 [08-24-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-cisco_callmanager!15

---

## 0.4.2 [08-14-2024]

* Changes made at 2024.08.14_19:09PM

See merge request itentialopensource/adapters/adapter-cisco_callmanager!14

---

## 0.4.1 [08-07-2024]

* Changes made at 2024.08.06_20:24PM

See merge request itentialopensource/adapters/adapter-cisco_callmanager!13

---

## 0.4.0 [07-05-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/itsm-testing/adapter-cisco_callmanager!12

---

## 0.3.6 [03-27-2024]

* Changes made at 2024.03.27_14:06PM

See merge request itentialopensource/adapters/itsm-testing/adapter-cisco_callmanager!11

---

## 0.3.5 [03-14-2024]

* Update metadata.json

See merge request itentialopensource/adapters/itsm-testing/adapter-cisco_callmanager!10

---

## 0.3.4 [03-13-2024]

* Changes made at 2024.03.13_15:06PM

See merge request itentialopensource/adapters/itsm-testing/adapter-cisco_callmanager!9

---

## 0.3.3 [03-13-2024]

* Changes made at 2024.03.13_11:11AM

See merge request itentialopensource/adapters/itsm-testing/adapter-cisco_callmanager!8

---

## 0.3.2 [03-11-2024]

* Changes made at 2024.03.11_14:47PM

See merge request itentialopensource/adapters/itsm-testing/adapter-cisco_callmanager!7

---

## 0.3.1 [02-28-2024]

* Changes made at 2024.02.28_11:12AM

See merge request itentialopensource/adapters/itsm-testing/adapter-cisco_callmanager!6

---

## 0.3.0 [12-31-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/itsm-testing/adapter-cisco_callmanager!5

---

## 0.2.0 [05-23-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/itsm-testing/adapter-cisco_callmanager!4

---

## 0.1.4 [03-02-2021]

- Migration to bring up to the latest foundation
	- Change to .eslintignore (adapter_modification directory)
	- Change to README.md (new properties, new scripts, new processes)
	- Changes to adapterBase.js (new methods)
	- Changes to package.json (new scripts, dependencies)
	- Changes to propertiesSchema.json (new properties and changes to existing)
	- Changes to the Unit test
	- Adding several test files, utils files and .generic entity
	- Fix order of scripts and dependencies in package.json
	- Fix order of properties in propertiesSchema.json
	- Update sampleProperties, unit and integration tests to have all new properties.
	- Add all new calls to adapter.js and pronghorn.json
	- Add suspend piece to older methods

See merge request itentialopensource/adapters/itsm-testing/adapter-cisco_callmanager!3

---

## 0.1.3 [02-05-2021]

- Fix issues from last migration

See merge request itentialopensource/adapters/itsm-testing/adapter-cisco_callmanager!2

---

## 0.1.2 [01-25-2021]

- Changes to change the entitypaths (generic endpoint) and handle XML data both in and out.
Changes for Basic Auth.
Also migrated to the latest adapter foundation

See merge request itentialopensource/adapters/itsm-testing/adapter-cisco_callmanager!1

---

## 0.1.1 [08-16-2020]

- Initial Commit

See commit d7df510

---
