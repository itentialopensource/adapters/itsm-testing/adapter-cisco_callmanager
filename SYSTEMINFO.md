# Cisco Call Manager

Vendor: Cisco Systems
Homepage: https://www.cisco.com/

Product: Unified Communications Managaer (Call Manager)
Product https://www.cisco.com/c/en/us/products/unified-communications/unified-communications-manager-callmanager/index.html

## Introduction
We classify Cisco Call Manager into the ITSM (Service Management) domain as Cisco Call Manager provides ticketing solutions for Call Management.

"Cisco Unified Communications Manager (Unified CM) provides reliable, secure, scalable, and manageable call control and session management." 

## Why Integrate
The Cisco Call Manager adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Cisco Call Manager. With this adapter you have the ability to perform operations such as:

- Add Phone
- Get Phone

## Additional Product Documentation
The [API documents for Cisco Call Manager](https://www.cisco.com/c/en/us/td/docs/voice_ip_comm/cucme/CUCIS_API/CUCIS_API_Guide/CUCISA_OVR.html)
The [Authentication for Cisco Call Manager](https://community.cisco.com/t5/collaboration-blogs/how-to-access-cisco-callmanager-cucm-with-axl-api-postman-tool/ba-p/4027261)
